use crate::engine::asset::{
    AssetEvent, AssetRef, AssetStorage, AssetStorageEvent, AssetStorageProcessResponse, Assets,
    Loaded,
};
use crate::engine::error::{EngineError, EngineResult};
use crate::engine::model::Vertex;
use crate::engine::render::{Uniforms, Viewport};
use crate::engine::texture::Texture;
use legion::prelude::*;
use relative_path::RelativePath;
use serde::{Deserialize, Serialize};
use std::fs;
use std::fs::File;
use std::io::BufReader;
use std::path::Path;
use try_block::try_block;
use tuple_combinator::TupleCombinator;
use wgpu::TextureFormat;

#[derive(Debug, Eq, PartialEq, Hash, Clone, Deserialize, Serialize)]
pub struct ShaderRef(AssetRef);

impl ShaderRef {
    #[inline]
    pub fn path(&self) -> &RelativePath {
        self.0.path()
    }

    #[inline]
    pub fn asset(&self) -> &AssetRef {
        &self.0
    }
}

impl<T: Into<AssetRef>> From<T> for ShaderRef {
    fn from(t: T) -> Self {
        Self(t.into())
    }
}

impl AsRef<AssetRef> for ShaderRef {
    fn as_ref(&self) -> &AssetRef {
        self.asset()
    }
}

pub type ShaderStorage = AssetStorage<wgpu::ShaderModule>;

pub struct Shaders;

impl Shaders {
    const SHADER_EXTENSIONS: &'static [&'static str] = &["frag", "vert"];

    pub fn storage() -> ShaderStorage {
        AssetStorage::new()
    }

    pub fn system() -> Box<dyn Schedulable> {
        SystemBuilder::new("ShadersSystem")
            .write_resource::<ShaderStorage>()
            .read_resource::<Assets>()
            .read_resource::<Viewport>()
            .build(
                move |_commands, _world, (shaders, assets, viewport), _query| {
                    for event in assets.events().iter().filter(|e| {
                        e.extension()
                            .filter(|ext| Self::SHADER_EXTENSIONS.contains(ext))
                            .is_some()
                    }) {
                        match event {
                            AssetEvent::Upsert(asset_ref) => shaders.upsert(asset_ref.clone()),
                            AssetEvent::Remove(asset_ref) => shaders.remove(asset_ref.clone()),
                        }
                    }

                    shaders.process(|asset_ref, tx| {
                        Self::load_shader(tx, asset_ref, assets.base_path(), &viewport)
                    });
                },
            )
    }

    fn load_shader(
        tx: crossbeam_channel::Sender<(
            AssetRef,
            EngineResult<AssetStorageProcessResponse<wgpu::ShaderModule>>,
        )>,
        asset_ref: AssetRef,
        base_path: &Path,
        viewport: &Viewport,
    ) {
        let res = try_block! {
            let kind = match asset_ref.path().extension() {
                Some("frag") => shaderc::ShaderKind::Fragment,
                Some("vert") => shaderc::ShaderKind::Vertex,
                _ => panic!("expect *.frag or *.vert shader file"),
            };

            let bytes = fs::read(asset_ref.path().to_path(base_path))
                .map_err(|e| EngineError::ShaderLoadError(e, asset_ref.path().to_owned()))?;

            let src = String::from_utf8_lossy(&bytes);

            // TODO: don't rebuild the compiler
            let mut compiler =
                shaderc::Compiler::new().ok_or_else(|| EngineError::ShaderCompilerInitError)?;

            let spirv = compiler.compile_into_spirv(
                &src,
                kind,
                asset_ref.path().file_name().unwrap(),
                "Shaders.load_shader",
                None,
            )?;

            let data = wgpu::read_spirv(std::io::Cursor::new(spirv.as_binary_u8()))
                .map_err(|e| EngineError::SpirvReadError(e))?;

            let module = viewport.device.create_shader_module(&data);
            Ok(AssetStorageProcessResponse::Loaded(Loaded::independent(module)))
        };

        tx.send((asset_ref, res)).unwrap();
    }
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Material {
    pub vert_shader: ShaderRef,
    pub frag_shader: ShaderRef,

    #[serde(skip)]
    pub render_pipeline: Option<wgpu::RenderPipeline>,
}

impl Material {
    pub fn build_pipeline(
        &mut self,
        device: &wgpu::Device,
        sc_texture_format: TextureFormat,
        vert_shader_module: &wgpu::ShaderModule,
        frag_shader_module: &wgpu::ShaderModule,
        render_pipeline_layout: &wgpu::PipelineLayout,
    ) {
        let render_pipeline = device.create_render_pipeline(&wgpu::RenderPipelineDescriptor {
            layout: render_pipeline_layout,
            vertex_stage: wgpu::ProgrammableStageDescriptor {
                module: vert_shader_module,
                entry_point: "main",
            },
            fragment_stage: Some(wgpu::ProgrammableStageDescriptor {
                module: frag_shader_module,
                entry_point: "main",
            }),
            rasterization_state: Some(wgpu::RasterizationStateDescriptor {
                front_face: wgpu::FrontFace::Ccw,
                cull_mode: wgpu::CullMode::Back,
                depth_bias: 0,
                depth_bias_slope_scale: 0.0,
                depth_bias_clamp: 0.0,
            }),
            color_states: &[wgpu::ColorStateDescriptor {
                format: sc_texture_format,
                color_blend: wgpu::BlendDescriptor::REPLACE,
                alpha_blend: wgpu::BlendDescriptor::REPLACE,
                write_mask: wgpu::ColorWrite::ALL,
            }],
            primitive_topology: wgpu::PrimitiveTopology::TriangleList,
            depth_stencil_state: Some(wgpu::DepthStencilStateDescriptor {
                format: Texture::DEPTH_FORMAT,
                depth_write_enabled: true,
                depth_compare: wgpu::CompareFunction::Less,
                stencil_front: wgpu::StencilStateFaceDescriptor::IGNORE,
                stencil_back: wgpu::StencilStateFaceDescriptor::IGNORE,
                stencil_read_mask: 0,
                stencil_write_mask: 0,
            }),
            vertex_state: wgpu::VertexStateDescriptor {
                index_format: wgpu::IndexFormat::Uint16,
                vertex_buffers: &[Vertex::desc()],
            },
            sample_count: 1,
            sample_mask: !0,
            alpha_to_coverage_enabled: false,
        });

        self.render_pipeline = Some(render_pipeline);
    }
}

#[derive(Debug, Eq, PartialEq, Hash, Clone, Deserialize, Serialize)]
pub struct MaterialRef(AssetRef);

impl MaterialRef {
    #[inline]
    pub fn path(&self) -> &RelativePath {
        self.0.path()
    }

    #[inline]
    pub fn asset(&self) -> &AssetRef {
        &self.0
    }
}

impl<T: Into<AssetRef>> From<T> for MaterialRef {
    fn from(t: T) -> Self {
        Self(t.into())
    }
}

impl AsRef<AssetRef> for MaterialRef {
    fn as_ref(&self) -> &AssetRef {
        self.asset()
    }
}

pub type MaterialStorage = AssetStorage<Material>;

pub struct Materials {
    pub diffuse_group_layout: wgpu::BindGroupLayout,
    pub uniform_group_layout: wgpu::BindGroupLayout,
    // TODO: move next 2 to a different asset?
    pub uniform_bind_group: wgpu::BindGroup,
    pub uniform_buffer: wgpu::Buffer,
    pub instance_group_layout: wgpu::BindGroupLayout,
    pub depth_texture: Texture,
    pub render_pipeline_layout: wgpu::PipelineLayout,
}

impl Materials {
    const MATERIAL_EXTENSIONS: &'static [&'static str] = &["mat"];

    pub fn new(viewport: &Viewport) -> Self {
        let diffuse_group_layout =
            viewport
                .device
                .create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
                    bindings: &[
                        wgpu::BindGroupLayoutEntry {
                            binding: 0,
                            visibility: wgpu::ShaderStage::FRAGMENT,
                            ty: wgpu::BindingType::SampledTexture {
                                multisampled: false,
                                dimension: wgpu::TextureViewDimension::D2,
                                component_type: wgpu::TextureComponentType::Uint,
                            },
                        },
                        wgpu::BindGroupLayoutEntry {
                            binding: 1,
                            visibility: wgpu::ShaderStage::FRAGMENT,
                            ty: wgpu::BindingType::Sampler { comparison: false },
                        },
                    ],
                    label: Some("diffuse_bind_group_layout"),
                });

        let uniform_group_layout =
            viewport
                .device
                .create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
                    bindings: &[wgpu::BindGroupLayoutEntry {
                        binding: 0,
                        visibility: wgpu::ShaderStage::VERTEX,
                        ty: wgpu::BindingType::UniformBuffer { dynamic: false },
                    }],
                    label: Some("uniform_bind_group_layout"),
                });

        let uniform_buffer = viewport.device.create_buffer_with_data(
            bytemuck::cast_slice(&[Uniforms::new()]),
            wgpu::BufferUsage::UNIFORM | wgpu::BufferUsage::COPY_DST,
        );

        let uniform_bind_group = viewport
            .device
            .create_bind_group(&wgpu::BindGroupDescriptor {
                layout: &uniform_group_layout,
                bindings: &[wgpu::Binding {
                    binding: 0,
                    resource: wgpu::BindingResource::Buffer {
                        buffer: &uniform_buffer,
                        // FYI: you can share a single buffer between bindings.
                        range: 0..std::mem::size_of::<Uniforms>() as wgpu::BufferAddress,
                    },
                }],
                label: Some("uniform_bind_group"),
            });

        let instance_group_layout =
            viewport
                .device
                .create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
                    bindings: &[wgpu::BindGroupLayoutEntry {
                        binding: 0,
                        visibility: wgpu::ShaderStage::VERTEX,
                        ty: wgpu::BindingType::StorageBuffer {
                            dynamic: false,
                            readonly: true,
                        },
                    }],
                    label: Some("instance_bind_group_layout"),
                });

        let depth_texture = Texture::create_depth_texture(
            &viewport.device,
            &viewport.sc_desc,
            &diffuse_group_layout,
        );

        let render_pipeline_layout =
            viewport
                .device
                .create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
                    bind_group_layouts: &[
                        &diffuse_group_layout,
                        &uniform_group_layout,
                        &instance_group_layout,
                    ],
                });

        Self {
            diffuse_group_layout,
            uniform_group_layout,
            uniform_bind_group,
            uniform_buffer,
            instance_group_layout,
            depth_texture,
            render_pipeline_layout,
        }
    }

    pub fn resize(&mut self, viewport: &Viewport) {
        self.depth_texture = Texture::create_depth_texture(
            &viewport.device,
            &viewport.sc_desc,
            &self.diffuse_group_layout,
        );
    }

    pub fn storage() -> MaterialStorage {
        AssetStorage::new()
    }

    pub fn system() -> Box<dyn Schedulable> {
        SystemBuilder::new("MaterialsSystem")
            .write_resource::<MaterialStorage>()
            .read_resource::<Assets>()
            .read_resource::<ShaderStorage>()
            .read_resource::<Materials>()
            .read_resource::<Viewport>()
            .build(
                move |_commands,
                      _world,
                      (material_storage, assets, shaders, materials, viewport),
                      _query| {
                    for event in assets.events().iter().filter(|e| {
                        e.extension()
                            .filter(|ext| Self::MATERIAL_EXTENSIONS.contains(ext))
                            .is_some()
                    }) {
                        match event {
                            AssetEvent::Upsert(asset_ref) => {
                                material_storage.upsert(asset_ref.clone())
                            }
                            AssetEvent::Remove(asset_ref) => {
                                material_storage.remove(asset_ref.clone())
                            }
                        }
                    }

                    for event in shaders.events() {
                        match event {
                            AssetStorageEvent::Loaded(shader_ref) => {
                                material_storage.upsert_for_dependents(shader_ref);
                            }
                            AssetStorageEvent::Removed(shader_ref) => {
                                material_storage.upsert_for_dependents(shader_ref);
                            }
                            AssetStorageEvent::Registered(shader_ref) => {
                                material_storage.upsert_for_dependents(shader_ref);
                            }
                        }
                    }

                    material_storage.process(|asset_ref, tx| {
                        Self::load_material(
                            tx,
                            asset_ref,
                            assets.base_path(),
                            materials,
                            shaders,
                            &viewport,
                        )
                    });
                },
            )
    }

    fn load_material(
        tx: crossbeam_channel::Sender<(
            AssetRef,
            EngineResult<AssetStorageProcessResponse<Material>>,
        )>,
        asset_ref: AssetRef,
        base_path: &Path,
        materials: &Materials,
        shaders: &ShaderStorage,
        viewport: &Viewport,
    ) {
        let res = try_block! {
            let file = File::open(asset_ref.path().to_path(base_path))
                .map_err(|e| EngineError::MaterialLoadError(e, asset_ref.path().to_owned()))?;
            let reader = BufReader::new(file);
            let mut material: Material = serde_json::from_reader(reader)
                .map_err(|e| EngineError::MaterialLoadError(e.into(), asset_ref.path().to_owned()))?;

            let vert_shader_module = shaders.get_or_queue(&material.vert_shader)?;
            let frag_shader_module = shaders.get_or_queue(&material.frag_shader)?;

            match (vert_shader_module, frag_shader_module).transpose() {
                Some((vert_shader_module, frag_shader_module)) => {
                    material.build_pipeline(
                        &viewport.device,
                        viewport.sc_desc.format,
                        vert_shader_module,
                        frag_shader_module,
                        &materials.render_pipeline_layout,
                    );

                    let dependencies = vec![
                        material.vert_shader.asset().clone(),
                        material.frag_shader.asset().clone()
                    ];
                    Ok(AssetStorageProcessResponse::Loaded(Loaded::new(material, dependencies)))
                }
                None => Ok(AssetStorageProcessResponse::Requeue)
            }
        };

        tx.send((asset_ref, res)).unwrap()
    }
}
