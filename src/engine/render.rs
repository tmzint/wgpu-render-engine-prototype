use crate::engine::base::Transform;
use crate::engine::error::{EngineError, EngineResult};
use crate::engine::input::{InputEvent, Inputs};
use crate::engine::material::{MaterialRef, MaterialStorage, Materials};
use crate::engine::model::{ModelRef, ModelStorage};
use crate::engine::texture::{TextureRef, TextureStorage};
use crate::global::FnvIndexMap;
use crate::unwrap_or_continue;
use cgmath::{Matrix4, Point3, Vector3};
use legion::prelude::*;
use std::borrow::BorrowMut;
use winit::window::Window;

// Coordinate system in wgpu uses a range of -1.0 to +1.0 for x and y with 0.0 to +1.0 for z.
// cgmath uses open gls model with ranges of 0.0 to 1.0.
#[cfg_attr(rustfmt, rustfmt_skip)]
pub const OPENGL_TO_WGPU_MATRIX: cgmath::Matrix4<f32> = cgmath::Matrix4::new(
    1.0, 0.0, 0.0, 0.0,
    0.0, 1.0, 0.0, 0.0,
    0.0, 0.0, 0.5, 0.0,
    0.0, 0.0, 0.5, 1.0,
);

pub struct Camera {
    pub eye: Point3<f32>,
    pub target: Point3<f32>,
    pub up: Vector3<f32>,
    pub scale: Vector3<f32>,
    pub aspect: f32,
    pub fovy: f32,
    pub znear: f32,
    pub zfar: f32,
}

impl Camera {
    pub fn build_view_projection_matrix(&self) -> cgmath::Matrix4<f32> {
        let view = cgmath::Matrix4::look_at(self.eye, self.target, self.up);
        let scale = cgmath::Matrix4::from_nonuniform_scale(
            1.0 / self.scale.x,
            1.0 / self.scale.y,
            1.0 / self.scale.z,
        );
        let proj = cgmath::perspective(cgmath::Deg(self.fovy), self.aspect, self.znear, self.zfar);
        return OPENGL_TO_WGPU_MATRIX * proj * view * scale;
    }
}

pub struct Render {
    pub model: ModelRef,
    pub texture: TextureRef,
    pub material: MaterialRef,
}

impl Render {
    pub fn new(model: ModelRef, texture: TextureRef, material: MaterialRef) -> Self {
        Render {
            model,
            texture,
            material,
        }
    }
}

pub struct Viewport {
    pub device: wgpu::Device,
    pub surface: wgpu::Surface,
    pub adapter: wgpu::Adapter,
    pub queue: wgpu::Queue,
    pub sc_desc: wgpu::SwapChainDescriptor,
    pub swap_chain: wgpu::SwapChain,
    pub size: winit::dpi::PhysicalSize<u32>,
    pub active_camera: Option<Camera>,
}

impl Viewport {
    pub async fn window(window: &Window) -> EngineResult<Self> {
        let size = window.inner_size();
        let surface = wgpu::Surface::create(window);

        let adapter = wgpu::Adapter::request(
            &wgpu::RequestAdapterOptions {
                power_preference: wgpu::PowerPreference::Default,
                compatible_surface: Some(&surface),
            },
            wgpu::BackendBit::PRIMARY, // Vulkan + Metal + DX12 + Browser WebGPU
        )
        .await
        .ok_or_else(|| EngineError::WgpuAdapterNotFound)?;

        let (device, queue) = adapter
            .request_device(&wgpu::DeviceDescriptor {
                extensions: wgpu::Extensions {
                    anisotropic_filtering: false,
                },
                limits: Default::default(),
            })
            .await;

        let sc_desc = wgpu::SwapChainDescriptor {
            usage: wgpu::TextureUsage::OUTPUT_ATTACHMENT,
            format: wgpu::TextureFormat::Bgra8UnormSrgb,
            width: size.width,
            height: size.height,
            present_mode: wgpu::PresentMode::Fifo,
        };
        let swap_chain = device.create_swap_chain(&surface, &sc_desc);

        Ok(Viewport {
            size,
            surface,
            adapter,
            device,
            queue,
            sc_desc,
            swap_chain,
            active_camera: None,
        })
    }

    pub fn aspect(&self) -> f32 {
        self.sc_desc.width as f32 / self.sc_desc.height as f32
    }

    pub fn resize(&mut self, new_size: winit::dpi::PhysicalSize<u32>) {
        self.size = new_size;
        self.sc_desc.width = new_size.width;
        self.sc_desc.height = new_size.height;
        self.swap_chain = self.device.create_swap_chain(&self.surface, &self.sc_desc);

        let aspect = self.aspect();
        if let Some(ref mut active_camera) = self.active_camera {
            active_camera.aspect = aspect;
        }
    }

    pub fn activate_camera(&mut self, mut camera: Camera) {
        camera.aspect = self.aspect();
        self.active_camera = Some(camera)
    }
}

pub struct ResizeViewportSystem;

impl ResizeViewportSystem {
    pub fn system() -> Box<dyn Schedulable> {
        SystemBuilder::new("ResizeViewportSystem")
            .read_resource::<Inputs>()
            .write_resource::<Viewport>()
            .write_resource::<Materials>()
            .build(
                move |_commands, _world, (inputs, viewport, materials), _query| {
                    for event in &inputs.events {
                        match event {
                            InputEvent::Resize(size) => {
                                viewport.resize(*size);
                                materials.resize(viewport);
                            }
                            _ => (),
                        }
                    }
                },
            )
    }
}

#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct Uniforms {
    view_proj: cgmath::Matrix4<f32>,
}

unsafe impl bytemuck::Pod for Uniforms {}
unsafe impl bytemuck::Zeroable for Uniforms {}

impl Uniforms {
    pub fn new() -> Self {
        use cgmath::SquareMatrix;
        Self {
            view_proj: cgmath::Matrix4::identity(),
        }
    }

    pub fn update_view_proj(&mut self, camera: &Camera) {
        self.view_proj = camera.build_view_projection_matrix();
    }
}

#[repr(C)]
#[derive(Debug, PartialEq, Copy, Clone)]
pub struct RenderInstance {
    transform: Matrix4<f32>,
    scale: Vector3<f32>,

    // required as vec3 still takes 4 bytes in glsl, see AsStd140
    _padding1: f32,
}

impl RenderInstance {
    pub fn new(transform: Matrix4<f32>, scale: Vector3<f32>) -> Self {
        RenderInstance {
            transform,
            scale,
            _padding1: 0.0,
        }
    }
}

unsafe impl bytemuck::Pod for RenderInstance {}
unsafe impl bytemuck::Zeroable for RenderInstance {}

#[derive(Debug, PartialEq, Default)]
pub struct RenderModels(FnvIndexMap<ModelRef, Vec<RenderInstance>>);

impl RenderModels {
    #[inline]
    pub fn add(&mut self, model: &ModelRef, instance: RenderInstance) {
        let instances: &mut Vec<RenderInstance> = self
            .0
            .entry(model.clone())
            .or_insert(Default::default())
            .borrow_mut();

        instances.push(instance);
    }

    pub fn iter(&self) -> impl Iterator<Item = (&ModelRef, &Vec<RenderInstance>)> {
        self.0.iter()
    }
}

#[derive(Debug, PartialEq, Default)]
pub struct RenderTextures(FnvIndexMap<TextureRef, RenderModels>);

impl RenderTextures {
    #[inline]
    pub fn add(&mut self, texture: &TextureRef, model: &ModelRef, instance: RenderInstance) {
        self.0
            .entry(texture.clone())
            .or_insert(Default::default())
            .borrow_mut()
            .add(model, instance);
    }

    pub fn iter(&self) -> impl Iterator<Item = (&TextureRef, &RenderModels)> {
        self.0.iter()
    }
}

#[derive(Debug, PartialEq, Default)]
pub struct RenderMaterials(FnvIndexMap<MaterialRef, RenderTextures>, usize);

impl RenderMaterials {
    #[inline]
    pub fn add(
        &mut self,
        material: &MaterialRef,
        texture: &TextureRef,
        model: &ModelRef,
        instance: RenderInstance,
    ) {
        self.0
            .entry(material.clone())
            .or_insert(Default::default())
            .borrow_mut()
            .add(texture, model, instance);

        self.1 += 1;
    }

    pub fn iter(&self) -> impl Iterator<Item = (&MaterialRef, &RenderTextures)> {
        self.0.iter()
    }

    pub fn iter_instances(&self) -> impl Iterator<Item = &RenderInstance> {
        self.0.iter().flat_map(|(_, textures)| {
            textures
                .0
                .iter()
                .flat_map(|(_, models)| models.0.iter().flat_map(|(_, i)| i.iter()))
        })
    }

    pub fn len_instances(&self) -> usize {
        self.1
    }
}

pub struct RenderSystem;

impl RenderSystem {
    pub fn system() -> Box<dyn Schedulable> {
        SystemBuilder::new("RenderSystem")
            .write_resource::<Viewport>()
            .read_resource::<TextureStorage>()
            .read_resource::<ModelStorage>()
            .read_resource::<Materials>()
            .read_resource::<MaterialStorage>()
            .with_query(<(TryRead<Transform>, Read<Render>)>::query())
            .build(
                move |_commands,
                      world,
                      (viewport, textures, models, materials, material_storage),
                      query| {
                    let frame = viewport
                        .swap_chain
                        .get_next_texture()
                        .expect("Timeout getting tree");

                    let mut encoder =
                        viewport
                            .device
                            .create_command_encoder(&wgpu::CommandEncoderDescriptor {
                                label: Some("Render Encoder 1"),
                            });

                    let _position_default = Transform::default();

                    let mut uniforms = Uniforms::new();
                    if let Some(ref active_camera) = viewport.active_camera {
                        uniforms.update_view_proj(active_camera);
                    }

                    // TODO: minimize buffer creation
                    let staging_buffer = viewport.device.create_buffer_with_data(
                        bytemuck::cast_slice(&[uniforms]),
                        wgpu::BufferUsage::COPY_SRC,
                    );

                    encoder.copy_buffer_to_buffer(
                        &staging_buffer,
                        0,
                        &materials.uniform_buffer,
                        0,
                        std::mem::size_of::<Uniforms>() as wgpu::BufferAddress,
                    );

                    // TODO: culling
                    // optimization: don't rebuild every frame
                    let mut render_materials = RenderMaterials::default();
                    for (transform, render) in query.iter(&mut *world) {
                        let transform = transform.map(|t| *t).unwrap_or_default();
                        render_materials.add(
                            &render.material,
                            &render.texture,
                            &render.model,
                            RenderInstance::new(transform.matrix(), transform.scale),
                        )
                    }

                    // clear frame
                    {
                        encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
                            color_attachments: &[wgpu::RenderPassColorAttachmentDescriptor {
                                attachment: &frame.view,
                                resolve_target: None,
                                load_op: wgpu::LoadOp::Clear,
                                store_op: wgpu::StoreOp::Store,
                                clear_color: wgpu::Color {
                                    r: 0.1,
                                    g: 0.2,
                                    b: 0.3,
                                    a: 1.0,
                                },
                            }],
                            depth_stencil_attachment: None,
                        });
                    }

                    // TODO: reuse buffer and bind group
                    // build instance data
                    let instance_buffer_size =
                        render_materials.len_instances() * std::mem::size_of::<RenderInstance>();

                    let instance_buffer = viewport.device.create_buffer_with_data(
                        bytemuck::cast_slice(
                            &render_materials
                                .iter_instances()
                                .copied()
                                .collect::<Vec<_>>(),
                        ),
                        wgpu::BufferUsage::STORAGE_READ,
                    );

                    let instance_bind_group =
                        viewport
                            .device
                            .create_bind_group(&wgpu::BindGroupDescriptor {
                                layout: &materials.instance_group_layout,
                                bindings: &[wgpu::Binding {
                                    binding: 0,
                                    resource: wgpu::BindingResource::Buffer {
                                        buffer: &instance_buffer,
                                        range: 0..instance_buffer_size as wgpu::BufferAddress,
                                    },
                                }],
                                label: Some("instance_bind_group"),
                            });

                    viewport.queue.submit(&[encoder.finish()]);

                    let mut encoder =
                        viewport
                            .device
                            .create_command_encoder(&wgpu::CommandEncoderDescriptor {
                                label: Some("Render Encoder 2"),
                            });

                    // render entities
                    {
                        let mut render_pass =
                            encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
                                color_attachments: &[wgpu::RenderPassColorAttachmentDescriptor {
                                    attachment: &frame.view,
                                    resolve_target: None,
                                    load_op: wgpu::LoadOp::Load,
                                    store_op: wgpu::StoreOp::Store,
                                    clear_color: wgpu::Color::TRANSPARENT,
                                }],
                                depth_stencil_attachment: Some(
                                    wgpu::RenderPassDepthStencilAttachmentDescriptor {
                                        attachment: &materials.depth_texture.view,
                                        depth_load_op: wgpu::LoadOp::Clear,
                                        depth_store_op: wgpu::StoreOp::Store,
                                        clear_depth: 1.0,
                                        stencil_load_op: wgpu::LoadOp::Clear,
                                        stencil_store_op: wgpu::StoreOp::Store,
                                        clear_stencil: 0,
                                    },
                                ),
                            });

                        render_pass.set_bind_group(1, &materials.uniform_bind_group, &[]);
                        for (material, render_textures) in render_materials.iter() {
                            // TODO: defaults // errors

                            let material = unwrap_or_continue!(material_storage
                                .get_or_queue(material)
                                .unwrap());
                            let render_pipeline =
                                unwrap_or_continue!(material.render_pipeline.as_ref());
                            render_pass.set_pipeline(render_pipeline);

                            for (texture, render_models) in render_textures.iter() {
                                // TODO: handle error
                                let texture =
                                    unwrap_or_continue!(textures.get_or_queue(texture).unwrap());
                                render_pass.set_bind_group(0, &texture.diffuse_bind_group, &[]);

                                // TODO: swap model and texture? requires benchmarking
                                for (model, instances) in render_models.iter() {
                                    let model =
                                        unwrap_or_continue!(models.get_or_queue(model).unwrap());
                                    let vertex_buffer =
                                        unwrap_or_continue!(model.vertex_buffer.as_ref());
                                    let index_buffer =
                                        unwrap_or_continue!(model.index_buffer.as_ref());
                                    render_pass.set_vertex_buffer(0, vertex_buffer, 0, 0);
                                    render_pass.set_index_buffer(index_buffer, 0, 0);
                                    render_pass.set_bind_group(2, &instance_bind_group, &[]);
                                    let len_instances = instances.len() as u32;
                                    for _instance in instances {
                                        render_pass.draw_indexed(
                                            0..model.indices.len() as u32,
                                            0,
                                            0..len_instances,
                                        );
                                    }
                                }
                            }
                        }
                    }

                    viewport.queue.submit(&[encoder.finish()]);
                },
            )
    }
}
