use crate::engine::asset::{
    AssetEvent, AssetRef, AssetStorage, AssetStorageProcessResponse, Assets, Loaded,
};
use crate::engine::error::{EngineError, EngineResult};
use crate::engine::material::Materials;
use crate::engine::render::Viewport;
use image::{GenericImageView, ImageFormat};
use legion::prelude::*;
use relative_path::RelativePath;
use serde::{Deserialize, Serialize};
use std::fs::File;
use std::io::BufReader;
use std::io::{BufRead, Seek};
use std::path::Path;
use try_block::try_block;

pub struct Texture {
    pub texture: wgpu::Texture,
    pub view: wgpu::TextureView,
    pub diffuse_bind_group: wgpu::BindGroup,
    pub sampler: wgpu::Sampler,
}

impl Texture {
    pub const DEPTH_FORMAT: wgpu::TextureFormat = wgpu::TextureFormat::Depth32Float;

    pub fn load<R: BufRead + Seek>(
        device: &wgpu::Device,
        queue: &wgpu::Queue,
        r: R,
        format: ImageFormat,
        diffuse_group_layout: &wgpu::BindGroupLayout,
    ) -> EngineResult<Self> {
        let img = image::load(r, format)?;
        Self::load_image(device, queue, &img, diffuse_group_layout)
    }

    pub fn load_image(
        device: &wgpu::Device,
        queue: &wgpu::Queue,
        img: &image::DynamicImage,
        diffuse_group_layout: &wgpu::BindGroupLayout,
    ) -> EngineResult<Self> {
        let rgba = img.as_rgba8().unwrap();
        let dimensions = img.dimensions();

        let size = wgpu::Extent3d {
            width: dimensions.0,
            height: dimensions.1,
            depth: 1,
        };
        let texture = device.create_texture(&wgpu::TextureDescriptor {
            size,
            array_layer_count: 1,
            mip_level_count: 1,
            sample_count: 1,
            dimension: wgpu::TextureDimension::D2,
            format: wgpu::TextureFormat::Rgba8UnormSrgb,
            usage: wgpu::TextureUsage::SAMPLED | wgpu::TextureUsage::COPY_DST,
            label: Some("tree"),
        });

        let buffer = device.create_buffer_with_data(&rgba, wgpu::BufferUsage::COPY_SRC);

        let mut encoder = device.create_command_encoder(&wgpu::CommandEncoderDescriptor {
            label: Some("texture_buffer_copy_encoder"),
        });

        encoder.copy_buffer_to_texture(
            wgpu::BufferCopyView {
                buffer: &buffer,
                offset: 0,
                bytes_per_row: 4 * dimensions.0,
                rows_per_image: dimensions.1,
            },
            wgpu::TextureCopyView {
                texture: &texture,
                mip_level: 0,
                array_layer: 0,
                origin: wgpu::Origin3d::ZERO,
            },
            size,
        );

        let cmd_buffer = encoder.finish();
        queue.submit(&[cmd_buffer]);

        let view = texture.create_default_view();

        let sampler = device.create_sampler(&wgpu::SamplerDescriptor {
            address_mode_u: wgpu::AddressMode::ClampToEdge,
            address_mode_v: wgpu::AddressMode::ClampToEdge,
            address_mode_w: wgpu::AddressMode::ClampToEdge,
            mag_filter: wgpu::FilterMode::Linear,
            min_filter: wgpu::FilterMode::Nearest,
            mipmap_filter: wgpu::FilterMode::Nearest,
            lod_min_clamp: -100.0,
            lod_max_clamp: 100.0,
            compare: wgpu::CompareFunction::Always,
        });

        let diffuse_bind_group = device.create_bind_group(&wgpu::BindGroupDescriptor {
            layout: &diffuse_group_layout,
            bindings: &[
                wgpu::Binding {
                    binding: 0,
                    resource: wgpu::BindingResource::TextureView(&view),
                },
                wgpu::Binding {
                    binding: 1,
                    resource: wgpu::BindingResource::Sampler(&sampler),
                },
            ],
            label: Some("diffuse_bind_group"),
        });

        Ok(Self {
            texture,
            view,
            diffuse_bind_group,
            sampler,
        })
    }

    pub fn create_depth_texture(
        device: &wgpu::Device,
        sc_desc: &wgpu::SwapChainDescriptor,
        diffuse_group_layout: &wgpu::BindGroupLayout,
    ) -> Self {
        let size = wgpu::Extent3d {
            width: sc_desc.width,
            height: sc_desc.height,
            depth: 1,
        };
        let desc = wgpu::TextureDescriptor {
            label: Some("depth_texture"),
            size,
            array_layer_count: 1,
            mip_level_count: 1,
            sample_count: 1,
            dimension: wgpu::TextureDimension::D2,
            format: Self::DEPTH_FORMAT,
            usage: wgpu::TextureUsage::OUTPUT_ATTACHMENT
                | wgpu::TextureUsage::SAMPLED
                | wgpu::TextureUsage::COPY_SRC,
        };
        let texture = device.create_texture(&desc);

        let view = texture.create_default_view();
        let sampler = device.create_sampler(&wgpu::SamplerDescriptor {
            address_mode_u: wgpu::AddressMode::ClampToEdge,
            address_mode_v: wgpu::AddressMode::ClampToEdge,
            address_mode_w: wgpu::AddressMode::ClampToEdge,
            mag_filter: wgpu::FilterMode::Linear,
            min_filter: wgpu::FilterMode::Nearest,
            mipmap_filter: wgpu::FilterMode::Nearest,
            lod_min_clamp: -100.0,
            lod_max_clamp: 100.0,
            compare: wgpu::CompareFunction::LessEqual,
        });

        let diffuse_bind_group = device.create_bind_group(&wgpu::BindGroupDescriptor {
            layout: &diffuse_group_layout,
            bindings: &[
                wgpu::Binding {
                    binding: 0,
                    resource: wgpu::BindingResource::TextureView(&view),
                },
                wgpu::Binding {
                    binding: 1,
                    resource: wgpu::BindingResource::Sampler(&sampler),
                },
            ],
            label: Some("depth_bind_group"),
        });

        Self {
            texture,
            view,
            diffuse_bind_group,
            sampler,
        }
    }
}

#[derive(Debug, Eq, PartialEq, Hash, Clone, Deserialize, Serialize)]
pub struct TextureRef(AssetRef);

impl TextureRef {
    #[inline]
    pub fn path(&self) -> &RelativePath {
        self.0.path()
    }

    #[inline]
    pub fn asset(&self) -> &AssetRef {
        &self.0
    }
}

impl<T: Into<AssetRef>> From<T> for TextureRef {
    fn from(t: T) -> Self {
        Self(t.into())
    }
}

impl AsRef<AssetRef> for TextureRef {
    fn as_ref(&self) -> &AssetRef {
        self.asset()
    }
}

pub type TextureStorage = AssetStorage<Texture>;

pub struct Textures;

impl Textures {
    const TEXTURE_EXTENSIONS: &'static [&'static str] = &["png"];

    pub fn storage() -> TextureStorage {
        AssetStorage::new()
    }

    pub fn system() -> Box<dyn Schedulable> {
        SystemBuilder::new("TexturesSystem")
            .write_resource::<TextureStorage>()
            .read_resource::<Assets>()
            .read_resource::<Materials>()
            .read_resource::<Viewport>()
            .build(
                move |_commands, _world, (textures, assets, materials, viewport), _query| {
                    for event in assets.events().iter().filter(|e| {
                        e.extension()
                            .filter(|ext| Self::TEXTURE_EXTENSIONS.contains(ext))
                            .is_some()
                    }) {
                        match event {
                            AssetEvent::Upsert(asset_ref) => textures.upsert(asset_ref.clone()),
                            AssetEvent::Remove(asset_ref) => textures.remove(asset_ref.clone()),
                        }
                    }

                    textures.process(|asset_ref, tx| {
                        Self::load_texture(tx, asset_ref, assets.base_path(), &materials, &viewport)
                    });
                },
            )
    }

    fn load_texture(
        tx: crossbeam_channel::Sender<(
            AssetRef,
            EngineResult<AssetStorageProcessResponse<Texture>>,
        )>,
        asset_ref: AssetRef,
        base_path: &Path,
        materials: &Materials,
        viewport: &Viewport,
    ) {
        let res = try_block! {
            let file = File::open(asset_ref.path().to_path(base_path))
                .map_err(|e| EngineError::TextureLoadError(e, asset_ref.path().to_owned()))?;
            let reader = BufReader::new(file);
            let format = ImageFormat::from_path(asset_ref.path().to_path("."))?;

            let texture = Texture::load(
                &viewport.device,
                &viewport.queue,
                reader,
                format,
                &materials.diffuse_group_layout,
            )?;

            Ok(AssetStorageProcessResponse::Loaded(Loaded::independent(texture)))
        };

        tx.send((asset_ref, res)).unwrap();
    }
}
