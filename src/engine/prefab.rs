// TODO: WIP, not working

use legion::filter::{ArchetypeFilterData, Filter};
use legion::iterator::SliceVecIter;
use legion::prelude::Entity;
use legion::storage::{
    ArchetypeDescription, ComponentMeta, ComponentStorage, ComponentTypeId, SliceVec,
};
use legion::world::{ComponentLayout, ComponentSource, ComponentTypeTupleSet, IntoComponentSource};
use std::ptr::NonNull;

pub struct DynamicComponents {
    layout: Vec<(ComponentTypeId, ComponentMeta)>,
    data: Vec<Vec<()>>, // TODO: this structure has to take ownership of the data
    progress: usize,
}

impl<'a> Filter<ArchetypeFilterData<'a>> for DynamicComponents {
    type Iter = SliceVecIter<'a, ComponentTypeId>;

    fn collect(&self, source: ArchetypeFilterData<'a>) -> Self::Iter {
        source.component_types.iter()
    }

    fn is_match(&self, item: &<Self::Iter as Iterator>::Item) -> Option<bool> {
        Some(self.layout.len() == item.len() && self.layout.iter().all(|(t, _)| item.contains(t)))
    }
}

impl ComponentLayout for DynamicComponents {
    type Filter = Self;

    fn get_filter(&mut self) -> &mut Self::Filter {
        self
    }

    fn tailor_archetype(&self, archetype: &mut ArchetypeDescription) {
        for (id, meta) in &self.layout {
            archetype.register_component_raw(id.clone(), meta.clone())
        }
    }
}

impl ComponentSource for DynamicComponents {
    fn is_empty(&mut self) -> bool {
        self.data.is_empty()
    }

    fn len(&self) -> usize {
        self.data.len()
    }

    fn write<T: Iterator<Item = Entity>>(
        &mut self,
        mut allocator: T,
        chunk: &mut ComponentStorage,
    ) -> usize {
        let space = chunk.capacity() - chunk.len();
        let mut writer = chunk.writer();
        let (entities, components) = writer.get();
        let mut count = 0;

        unsafe {
            for i in self.progress..self.data.len() {
                if count == space {
                    break;
                }

                self.data


                for j in 0..self.layout.len() {
                    let entity = allocator.next().unwrap();
                    entities.push(entity);

                    let (component_id, meta) = self.layout[j];

                    let mut component_writer = (&mut *components.get())
                        .get_mut(component_id)
                        .unwrap()
                        .writer();

                    let component_idx = j + i * self.layout.len();

                    //component_writer.push_raw(self.data[component_idx], 1)
                    // TODO
                }

                self.progress += 1;
            }
        }

        if self.progress == self.data.len() {
            // TODO: take ownership in iteration.
            std::mem::forget(&self.data);
        }

        unimplemented!();
        count
    }
}

impl IntoComponentSource for DynamicComponents {
    type Source = DynamicComponents;

    fn into(self) -> Self::Source {
        self
    }
}
