use legion::prelude::*;
use winit::event::WindowEvent;

#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum InputEvent {
    Resize(winit::dpi::PhysicalSize<u32>),
    KeyboardInput {
        device_id: winit::event::DeviceId,
        input: winit::event::KeyboardInput,
        is_synthetic: bool,
    },
}

impl InputEvent {
    pub fn resolve(event: &winit::event::WindowEvent) -> Option<InputEvent> {
        match event {
            WindowEvent::Resized(physical_size) => Some(InputEvent::Resize(*physical_size)),
            WindowEvent::ScaleFactorChanged { new_inner_size, .. } => {
                Some(InputEvent::Resize(**new_inner_size))
            }
            WindowEvent::KeyboardInput {
                device_id,
                input,
                is_synthetic,
            } => Some(InputEvent::KeyboardInput {
                device_id: *device_id,
                input: *input,
                is_synthetic: *is_synthetic,
            }),
            WindowEvent::ModifiersChanged(_) => None,
            WindowEvent::CursorMoved { .. } => None,
            WindowEvent::CursorEntered { .. } => None,
            WindowEvent::CursorLeft { .. } => None,
            WindowEvent::MouseWheel { .. } => None,
            WindowEvent::MouseInput { .. } => None,
            WindowEvent::AxisMotion { .. } => None,
            WindowEvent::Touch(_) => None,
            _ => None,
        }
    }
}

#[derive(Default)]
pub struct Inputs {
    pub events: Vec<InputEvent>,
}

impl Inputs {
    pub fn queue(&mut self, event: InputEvent) {
        self.events.push(event);
    }

    pub fn fetch_system(events: crossbeam_channel::Receiver<InputEvent>) -> Box<dyn Schedulable> {
        SystemBuilder::new("InputsFetchSystem")
            .write_resource::<Inputs>()
            .build(move |_commands, _world, inputs, _query| {
                for input in events.try_iter() {
                    inputs.queue(input);
                }
            })
    }

    pub fn clear_system() -> Box<dyn Schedulable> {
        SystemBuilder::new("InputsClearSystem")
            .write_resource::<Inputs>()
            .build(move |_commands, _world, inputs, _query| {
                inputs.events.clear();
            })
    }
}
