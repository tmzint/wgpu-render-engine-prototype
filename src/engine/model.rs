use crate::engine::asset::{
    AssetEvent, AssetRef, AssetStorage, AssetStorageProcessResponse, Assets, Loaded,
};
use crate::engine::error::{EngineError, EngineResult};
use crate::engine::render::Viewport;
use legion::prelude::*;
use relative_path::RelativePath;
use serde::{Deserialize, Serialize};
use std::fs::File;
use std::io::BufReader;
use std::path::Path;
use try_block::try_block;

#[repr(C)]
#[derive(Copy, Clone, Debug, Serialize, Deserialize)]
pub struct Vertex {
    position: [f32; 3],
    tex_coords: [f32; 2],
}

impl Vertex {
    pub fn desc<'a>() -> wgpu::VertexBufferDescriptor<'a> {
        use std::mem;
        wgpu::VertexBufferDescriptor {
            stride: mem::size_of::<Vertex>() as wgpu::BufferAddress,
            step_mode: wgpu::InputStepMode::Vertex,
            attributes: &[
                wgpu::VertexAttributeDescriptor {
                    offset: 0,
                    shader_location: 0,
                    format: wgpu::VertexFormat::Float3,
                },
                wgpu::VertexAttributeDescriptor {
                    offset: mem::size_of::<[f32; 3]>() as wgpu::BufferAddress,
                    shader_location: 1,
                    format: wgpu::VertexFormat::Float2,
                },
            ],
        }
    }
}

unsafe impl bytemuck::Pod for Vertex {}
unsafe impl bytemuck::Zeroable for Vertex {}

#[derive(Debug, Serialize, Deserialize)]
pub struct Model {
    pub vertices: Vec<Vertex>,
    pub indices: Vec<u16>,

    #[serde(skip)]
    pub vertex_buffer: Option<wgpu::Buffer>,
    #[serde(skip)]
    pub index_buffer: Option<wgpu::Buffer>,
}

impl Model {
    pub fn build_buffers(&mut self, device: &wgpu::Device) {
        let vertex_buffer = device.create_buffer_with_data(
            bytemuck::cast_slice(&self.vertices),
            wgpu::BufferUsage::VERTEX,
        );
        self.vertex_buffer = Some(vertex_buffer);

        let index_buffer = device.create_buffer_with_data(
            bytemuck::cast_slice(&self.indices),
            wgpu::BufferUsage::INDEX,
        );
        self.index_buffer = Some(index_buffer);
    }
}

#[derive(Debug, Eq, PartialEq, Hash, Clone, Deserialize, Serialize)]
pub struct ModelRef(AssetRef);

impl ModelRef {
    #[inline]
    pub fn path(&self) -> &RelativePath {
        self.0.path()
    }

    #[inline]
    pub fn asset(&self) -> &AssetRef {
        &self.0
    }
}

impl<T: Into<AssetRef>> From<T> for ModelRef {
    fn from(t: T) -> Self {
        Self(t.into())
    }
}

impl AsRef<AssetRef> for ModelRef {
    fn as_ref(&self) -> &AssetRef {
        self.asset()
    }
}

pub type ModelStorage = AssetStorage<Model>;

pub struct Models;

impl Models {
    const MODEL_EXTENSIONS: &'static [&'static str] = &["mod"];

    pub fn storage() -> ModelStorage {
        AssetStorage::new()
    }

    pub fn system() -> Box<dyn Schedulable> {
        SystemBuilder::new("ModelsSystem")
            .write_resource::<ModelStorage>()
            .read_resource::<Assets>()
            .read_resource::<Viewport>()
            .build(
                move |_commands, _world, (models, assets, viewport), _query| {
                    for event in assets.events().iter().filter(|e| {
                        e.extension()
                            .filter(|ext| Self::MODEL_EXTENSIONS.contains(ext))
                            .is_some()
                    }) {
                        match event {
                            AssetEvent::Upsert(asset_ref) => models.upsert(asset_ref.clone()),
                            AssetEvent::Remove(asset_ref) => models.remove(asset_ref.clone()),
                        }
                    }

                    models.process(|asset_ref, tx| {
                        Self::load_model(tx, asset_ref, assets.base_path(), &viewport)
                    });
                },
            )
    }

    fn load_model(
        tx: crossbeam_channel::Sender<(AssetRef, EngineResult<AssetStorageProcessResponse<Model>>)>,
        asset_ref: AssetRef,
        base_path: &Path,
        viewport: &Viewport,
    ) {
        let res = try_block! {
            let file = File::open(asset_ref.path().to_path(base_path))
                .map_err(|e| EngineError::ModelLoadError(e, asset_ref.path().to_owned()))?;
            let reader = BufReader::new(file);
            let mut model: Model = serde_json::from_reader(reader)
                .map_err(|e| EngineError::ModelLoadError(e.into(), asset_ref.path().to_owned()))?;

            model.build_buffers(&viewport.device);

            Ok(AssetStorageProcessResponse::Loaded(Loaded::independent(model)))
        };

        tx.send((asset_ref, res)).unwrap();
    }
}
