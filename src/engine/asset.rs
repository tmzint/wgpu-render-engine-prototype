use crate::engine::error::{EngineError, EngineResult};
use crate::global::FnvIndexMap;
use crossbeam_channel::Receiver;
use fnv::FnvHashMap;
use indexmap::map::Entry;
use legion::prelude::*;
use notify::event::{AccessKind, AccessMode, CreateKind, ModifyKind, RemoveKind, RenameMode};
use notify::EventKind;
use relative_path::{RelativePath, RelativePathBuf};
use serde::export::fmt::Debug;
use serde::{Deserialize, Serialize};
use std::hash::Hash;
use std::ops::{Deref, DerefMut};
use std::path::{Path, PathBuf};
use std::sync::Mutex;
use walkdir::WalkDir;

#[derive(Debug, Clone, Eq, PartialEq, Hash, Serialize, Deserialize, Ord, PartialOrd)]
pub struct AssetRef(RelativePathBuf);

impl AssetRef {
    #[inline]
    pub fn path(&self) -> &RelativePath {
        self.0.as_relative_path()
    }
}

impl<T: Into<RelativePathBuf>> From<T> for AssetRef {
    fn from(t: T) -> Self {
        Self(t.into())
    }
}

impl AsRef<AssetRef> for AssetRef {
    fn as_ref(&self) -> &AssetRef {
        self
    }
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub enum AssetEvent {
    Upsert(AssetRef),
    Remove(AssetRef),
}

impl AssetEvent {
    pub fn extension(&self) -> Option<&str> {
        match self {
            AssetEvent::Upsert(r) => r.path().extension(),
            AssetEvent::Remove(r) => r.path().extension(),
        }
    }
}

#[derive(Debug)]
pub struct Assets {
    base_path: PathBuf,
    events: Vec<AssetEvent>,
}

impl Assets {
    pub fn init(base_path: PathBuf, asset_dir: &RelativePath) -> Self {
        let mut assets = Self {
            base_path,
            events: Vec::default(),
        };
        assets.init_events(asset_dir);
        assets
    }

    #[inline]
    pub fn base_path(&self) -> &Path {
        &self.base_path
    }

    pub fn system(resource_changes: Receiver<notify::Event>) -> Box<dyn Schedulable> {
        let mut is_init = false;

        SystemBuilder::new("AssetsSystem")
            .write_resource::<Assets>()
            .build(move |_commands, _world, assets, _query| {
                if !is_init {
                    is_init = true;
                } else {
                    assets.events.clear();
                }

                for event in resource_changes.try_iter() {
                    assets.push_into(event);
                }
            })
    }

    fn init_events(&mut self, dir: &RelativePath) {
        let base_path = self.base_path.clone();
        let initial = WalkDir::new(dir.to_path(&self.base_path))
            .into_iter()
            .filter_map(|e| e.ok())
            .filter(|e| e.file_type().is_file())
            .map(|file| {
                let resource = Self::as_relative_path(&base_path, file.path());
                AssetEvent::Upsert(resource.into())
            });

        self.events.extend(initial);
    }

    #[inline]
    pub fn events(&self) -> &[AssetEvent] {
        &self.events
    }

    #[inline]
    fn push_into(&mut self, underlying: notify::Event) {
        match underlying.kind {
            EventKind::Create(CreateKind::File) => {
                for p in underlying.paths {
                    self.events.push(AssetEvent::Upsert(
                        Self::as_relative_path(&self.base_path, &p).into(),
                    ));
                }
            }
            EventKind::Modify(ModifyKind::Name(RenameMode::To)) => {
                for p in underlying.paths {
                    self.events.push(AssetEvent::Upsert(
                        Self::as_relative_path(&self.base_path, &p).into(),
                    ));
                }
            }
            EventKind::Modify(ModifyKind::Name(RenameMode::From)) => {
                for p in underlying.paths {
                    self.events.push(AssetEvent::Remove(
                        Self::as_relative_path(&self.base_path, &p).into(),
                    ));
                }
            }
            EventKind::Modify(ModifyKind::Name(RenameMode::Both)) => {
                if let Some(from) = underlying.paths.get(0) {
                    self.events.push(AssetEvent::Remove(
                        Self::as_relative_path(&self.base_path, &from).into(),
                    ));
                }
                if let Some(to) = underlying.paths.get(1) {
                    self.events.push(AssetEvent::Upsert(
                        Self::as_relative_path(&self.base_path, &to).into(),
                    ));
                }
            }
            EventKind::Access(AccessKind::Close(AccessMode::Write)) => {
                for p in underlying.paths {
                    self.events.push(AssetEvent::Upsert(
                        Self::as_relative_path(&self.base_path, &p).into(),
                    ));
                }
            }
            EventKind::Remove(RemoveKind::File) => {
                for p in underlying.paths {
                    self.events.push(AssetEvent::Remove(
                        Self::as_relative_path(&self.base_path, &p).into(),
                    ));
                }
            }
            _ => (),
        }
    }

    #[inline]
    fn as_relative_path(base_path: &Path, path: &Path) -> RelativePathBuf {
        RelativePathBuf::from_path(path.strip_prefix(base_path).unwrap()).unwrap()
    }
}

#[derive(Debug)]
pub struct Loaded<T> {
    asset: T,
    depends_on: Vec<AssetRef>,
}

impl<T> Loaded<T> {
    pub fn new(asset: T, mut depends_on: Vec<AssetRef>) -> Self {
        depends_on.sort();
        Self { asset, depends_on }
    }

    pub fn independent(asset: T) -> Self {
        Self {
            asset,
            depends_on: Vec::default(),
        }
    }
}

#[derive(Debug)]
pub enum AssetEntry<T> {
    Loaded(Loaded<T>),
    Loading,
    Unloaded,
}

impl<T> AssetEntry<T> {
    pub fn as_loaded(&self) -> Option<&Loaded<T>> {
        match self {
            AssetEntry::Loaded(loaded) => Some(loaded),
            AssetEntry::Loading => None,
            AssetEntry::Unloaded => None,
        }
    }
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub enum AssetStorageEvent {
    Loaded(AssetRef),
    Removed(AssetRef),
    Registered(AssetRef),
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub enum AssetStorageCommand {
    Load,
    Remove,
    Upsert,
}

#[derive(Debug)]
pub enum AssetStorageProcessResponse<T> {
    Loaded(Loaded<T>),
    Requeue,
}

pub struct AssetStorage<T: Sync + Send + 'static> {
    entries: FnvIndexMap<AssetRef, AssetEntry<T>>,
    command_queue: Mutex<FnvIndexMap<AssetRef, AssetStorageCommand>>,
    events: Vec<AssetStorageEvent>,
    load_rx: crossbeam_channel::Receiver<(AssetRef, EngineResult<AssetStorageProcessResponse<T>>)>,
    load_tx: crossbeam_channel::Sender<(AssetRef, EngineResult<AssetStorageProcessResponse<T>>)>,
    dependency_of: FnvHashMap<AssetRef, Vec<AssetRef>>,
}

impl<T: Sync + Send + 'static> AssetStorage<T> {
    pub fn new() -> Self {
        let (load_tx, load_rx) = crossbeam_channel::unbounded();

        Self {
            entries: Default::default(),
            command_queue: Mutex::new(Default::default()),
            events: Default::default(),
            load_rx,
            load_tx,
            dependency_of: Default::default(),
        }
    }

    #[inline]
    pub fn get<A: AsRef<AssetRef>>(&self, asset_ref: A) -> EngineResult<&AssetEntry<T>> {
        let asset_ref = asset_ref.as_ref();
        self.entries
            .get(asset_ref)
            .ok_or_else(|| EngineError::AssetNotFound(asset_ref.path().to_owned()))
    }

    #[inline]
    pub fn get_or_queue<A: AsRef<AssetRef>>(&self, asset_ref: A) -> EngineResult<Option<&T>> {
        let asset_ref = asset_ref.as_ref();
        let entry = self.get(asset_ref)?;
        let t = match entry {
            AssetEntry::Loaded(t) => Some(&t.asset),
            AssetEntry::Loading => None,
            AssetEntry::Unloaded => {
                self.command(asset_ref.clone(), AssetStorageCommand::Load);
                None
            }
        };
        Ok(t)
    }

    pub fn remove<A: Into<AssetRef>>(&mut self, asset_ref: A) {
        let asset_ref = asset_ref.into();
        self.command(asset_ref, AssetStorageCommand::Remove);
    }

    pub fn upsert<A: Into<AssetRef>>(&mut self, asset_ref: A) {
        let asset_ref = asset_ref.into();
        self.command(asset_ref, AssetStorageCommand::Upsert);
    }

    fn command<A: Into<AssetRef>>(&self, asset_ref: A, command: AssetStorageCommand) {
        let asset_ref = asset_ref.into();
        match self
            .command_queue
            .lock()
            .unwrap()
            .deref_mut()
            .entry(asset_ref)
        {
            Entry::Occupied(mut entry) => match entry.get_mut() {
                current @ AssetStorageCommand::Load => match command {
                    AssetStorageCommand::Load => {}
                    AssetStorageCommand::Remove => {
                        *current = AssetStorageCommand::Remove;
                    }
                    AssetStorageCommand::Upsert => {}
                },
                current @ AssetStorageCommand::Remove => match command {
                    AssetStorageCommand::Load => {}
                    AssetStorageCommand::Remove => {}
                    AssetStorageCommand::Upsert => {
                        *current = AssetStorageCommand::Upsert;
                    }
                },
                current @ AssetStorageCommand::Upsert => match command {
                    AssetStorageCommand::Load => {}
                    AssetStorageCommand::Remove => {
                        *current = AssetStorageCommand::Remove;
                    }
                    AssetStorageCommand::Upsert => {}
                },
            },
            Entry::Vacant(entry) => {
                entry.insert(command);
            }
        }
    }

    pub fn process<F>(&mut self, load: F)
    where
        F: Fn(
            AssetRef,
            crossbeam_channel::Sender<(AssetRef, EngineResult<AssetStorageProcessResponse<T>>)>,
        ),
    {
        self.events.clear();
        if !self.command_queue.lock().unwrap().deref().is_empty() {
            println!(
                "asset command queue: {:?}",
                self.command_queue.lock().unwrap().deref()
            );
        }

        self.interpret_commands(load);
        self.store_loaded();
    }

    fn interpret_commands<F>(&mut self, load: F)
    where
        F: Fn(
            AssetRef,
            crossbeam_channel::Sender<(AssetRef, EngineResult<AssetStorageProcessResponse<T>>)>,
        ),
    {
        for (asset_ref, command) in self.command_queue.lock().unwrap().deref_mut().drain(..) {
            match command {
                AssetStorageCommand::Load => {
                    if let Some(e @ AssetEntry::Unloaded) = self.entries.get_mut(&asset_ref) {
                        *e = AssetEntry::Loading;
                    }
                    load(asset_ref, self.load_tx.clone());
                }
                AssetStorageCommand::Remove => {
                    let prev = self.entries.remove(&asset_ref);
                    if let Some(prev) = prev.as_ref().and_then(|prev| prev.as_loaded()) {
                        Self::remove_depends_on(
                            &mut self.dependency_of,
                            &asset_ref,
                            &prev.depends_on,
                        );
                    }

                    self.events.push(AssetStorageEvent::Removed(asset_ref))
                }
                AssetStorageCommand::Upsert => {
                    match self.entries.get(&asset_ref) {
                        Some(AssetEntry::Unloaded) => {
                            continue;
                        }
                        Some(AssetEntry::Loaded(_)) => {
                            load(asset_ref, self.load_tx.clone());
                            continue;
                        }
                        Some(AssetEntry::Loading) => {
                            load(asset_ref, self.load_tx.clone());
                            continue;
                        }
                        None => (),
                    };

                    self.entries.insert(asset_ref.clone(), AssetEntry::Unloaded);
                    self.events.push(AssetStorageEvent::Registered(asset_ref))
                }
            }
        }
    }

    fn store_loaded(&mut self) {
        for (asset_ref, load_res) in self.load_rx.try_iter() {
            println!("asset store loaded: {:?}", asset_ref);

            // TODO: handle error
            let load_res = load_res.unwrap();
            if !self.entries.contains_key(&asset_ref) {
                continue;
            }

            match load_res {
                AssetStorageProcessResponse::Loaded(t) => {
                    let prev = self
                        .entries
                        .insert(asset_ref.clone(), AssetEntry::Loaded(t));

                    let prev: Option<&Loaded<T>> = prev.as_ref().and_then(|curr| curr.as_loaded());

                    let curr: &Loaded<T> = self
                        .entries
                        .get(&asset_ref)
                        .and_then(|curr| curr.as_loaded())
                        .unwrap();

                    match prev {
                        Some(prev) if prev.depends_on != curr.depends_on => {
                            Self::remove_depends_on(
                                &mut self.dependency_of,
                                &asset_ref,
                                &prev.depends_on,
                            );

                            for parent in &curr.depends_on {
                                let asset_dependencies =
                                    self.dependency_of.entry(parent.clone()).or_default();
                                asset_dependencies.push(asset_ref.clone());
                            }
                        }
                        None if !curr.depends_on.is_empty() => {
                            for parent in &curr.depends_on {
                                let asset_dependencies =
                                    self.dependency_of.entry(parent.clone()).or_default();
                                asset_dependencies.push(asset_ref.clone());
                            }
                        }
                        _ => {}
                    }

                    self.events.push(AssetStorageEvent::Loaded(asset_ref));
                }
                AssetStorageProcessResponse::Requeue => {
                    self.command(asset_ref, AssetStorageCommand::Load);
                }
            }
        }
    }

    fn remove_depends_on(
        dependencies: &mut FnvHashMap<AssetRef, Vec<AssetRef>>,
        from_asset_ref: &AssetRef,
        depends_ons: &[AssetRef],
    ) {
        for depends_on in depends_ons {
            let asset_dependencies = dependencies.entry(depends_on.clone()).or_default();
            let pos = asset_dependencies
                .iter()
                .position(|ad| ad == from_asset_ref);
            if let Some(pos) = pos {
                asset_dependencies.remove(pos);
            }
        }
    }

    pub fn events(&self) -> &[AssetStorageEvent] {
        &self.events
    }

    pub fn upsert_for_dependents(&mut self, parent: &AssetRef) {
        for dependent in self
            .dependency_of
            .get(parent)
            .into_iter()
            .flat_map(|v| v.iter())
        {
            self.command(dependent.clone(), AssetStorageCommand::Upsert);
        }
    }
}
