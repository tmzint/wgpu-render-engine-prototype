use cgmath::prelude::*;
use cgmath::{Matrix4, Quaternion, Vector3};

#[derive(Debug, Clone, Copy, PartialEq, Default)]
pub struct Frame {
    pub count: u64,
    pub elapsed: f64,
    pub delta: f32,
}

impl Frame {
    pub fn advance(&mut self, delta: f32) {
        self.count += 1;
        self.elapsed += delta as f64;
        self.delta = delta;
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Transform {
    pub translation: Vector3<f32>,
    pub rotation: Quaternion<f32>,
    pub scale: Vector3<f32>,
}

impl Transform {
    #[inline]
    pub fn matrix(&self) -> Matrix4<f32> {
        Matrix4::from_translation(self.translation) * Matrix4::from(self.rotation)
    }
}

impl Default for Transform {
    fn default() -> Self {
        Transform {
            translation: Vector3 {
                x: 0.0,
                y: 0.0,
                z: 0.0,
            },
            rotation: Quaternion::from_axis_angle(cgmath::Vector3::unit_z(), cgmath::Deg(0.0)),
            scale: Vector3 {
                x: 1.0,
                y: 1.0,
                z: 1.0,
            },
        }
    }
}
