use core::fmt;
use image::ImageError;
use relative_path::RelativePathBuf;
use serde::export::Formatter;
use winit::error::OsError;

pub type EngineResult<T> = Result<T, EngineError>;

#[derive(Debug)]
pub enum EngineError {
    WindowError(OsError),
    WgpuAdapterNotFound,
    NotifyError(notify::Error),
    ImageError(ImageError),
    ShaderCompilerInitError,
    ShaderLoadError(std::io::Error, RelativePathBuf),
    ShaderCompileError(shaderc::Error),
    SpirvReadError(std::io::Error),
    TextureLoadError(std::io::Error, RelativePathBuf),
    MaterialLoadError(std::io::Error, RelativePathBuf),
    ModelLoadError(std::io::Error, RelativePathBuf),
    AssetNotFound(RelativePathBuf),
}

impl From<notify::Error> for EngineError {
    fn from(e: notify::Error) -> Self {
        EngineError::NotifyError(e)
    }
}

impl From<OsError> for EngineError {
    fn from(e: OsError) -> Self {
        EngineError::WindowError(e)
    }
}

impl From<ImageError> for EngineError {
    fn from(e: ImageError) -> Self {
        EngineError::ImageError(e)
    }
}

impl From<shaderc::Error> for EngineError {
    fn from(e: shaderc::Error) -> Self {
        EngineError::ShaderCompileError(e)
    }
}

impl fmt::Display for EngineError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            EngineError::WindowError(e) => write!(f, "WindowError: {}", e),
            EngineError::NotifyError(e) => write!(f, "NotifyError: {}", e),
            EngineError::ImageError(e) => write!(f, "ImageError: {}", e),
            EngineError::WgpuAdapterNotFound => write!(f, "WgpuAdapterNotFound"),
            EngineError::ShaderCompilerInitError => write!(f, "ShaderCompilerInitError"),
            EngineError::ShaderCompileError(e) => write!(f, "ShaderCompileError: {}", e),
            EngineError::ShaderLoadError(e, path) => {
                write!(f, "ShaderLoadError for {}: {}", path, e)
            }
            EngineError::SpirvReadError(e) => write!(f, "SpirvReadError: {}", e),
            EngineError::TextureLoadError(e, path) => {
                write!(f, "TextureLoadError for {}: {}", path, e)
            }
            EngineError::MaterialLoadError(e, path) => {
                write!(f, "MaterialLoadError for {}: {}", path, e)
            }
            EngineError::ModelLoadError(e, path) => write!(f, "ModelLoadError for {}: {}", path, e),
            EngineError::AssetNotFound(p) => write!(f, "AssetNotFound: {}", p),
        }
    }
}
