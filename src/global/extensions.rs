pub trait BoolExt {
    fn iff<T, F>(self, f: F) -> Option<T>
    where
        F: FnOnce() -> T;

    fn and_iff<T, F>(self, f: F) -> Option<T>
    where
        F: FnOnce() -> Option<T>;

    fn assert<Err, F>(self, f: F) -> Result<(), Err>
    where
        F: FnOnce() -> Err;
}

impl BoolExt for bool {
    fn iff<T, F>(self, f: F) -> Option<T>
    where
        F: FnOnce() -> T,
    {
        if self {
            Some(f())
        } else {
            None
        }
    }

    fn and_iff<T, F>(self, f: F) -> Option<T>
    where
        F: FnOnce() -> Option<T>,
    {
        if self {
            f()
        } else {
            None
        }
    }

    fn assert<Err, F>(self, f: F) -> Result<(), Err>
    where
        F: FnOnce() -> Err,
    {
        if self {
            Ok(())
        } else {
            Err(f())
        }
    }
}

pub trait OptionExt<T> {
    fn len(&self) -> usize;
    fn zip<U>(self, other: Option<U>) -> Option<(T, U)>;
}

impl<T> OptionExt<T> for Option<T> {
    fn len(&self) -> usize {
        match self {
            Some(_) => 1,
            None => 0,
        }
    }

    fn zip<U>(self, other: Option<U>) -> Option<(T, U)> {
        self.and_then(|t| other.map(|u| (t, u)))
    }
}

pub trait ResultExt<T, E> {
    fn sequence(self) -> Option<Result<T, E>>;
}

impl<T, E> ResultExt<T, E> for Result<Option<T>, E> {
    fn sequence(self) -> Option<Result<T, E>> {
        match self {
            Ok(v) => v.map(|t| Ok(t)),
            Err(e) => Some(Err(e)),
        }
    }
}
