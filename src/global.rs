mod extensions;

pub use extensions::*;
use fnv::FnvBuildHasher;
use indexmap::map::IndexMap;
use indexmap::set::IndexSet;
use priority_queue::PriorityQueue;

pub type FnvIndexMap<K, T> = IndexMap<K, T, FnvBuildHasher>;
pub type FnvIndexSet<T> = IndexSet<T, FnvBuildHasher>;
pub type FnvPriorityQueue<T, P> = PriorityQueue<T, P, FnvBuildHasher>;

#[macro_export]
macro_rules! unwrap_or_continue {
    ($res:expr) => {
        match $res {
            Some(val) => val,
            None => {
                continue;
            }
        }
    };
}
