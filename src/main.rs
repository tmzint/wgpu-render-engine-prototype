#![allow(dead_code)]

mod engine;
mod game;
#[macro_use]
mod global;

use crate::engine::error::EngineResult;
use crate::engine::{Engine, Runtime, RuntimeBuilder, WindowConfig};
use crate::game::{ActionSystem, Actions, CameraControllerSystem, GameState, InitState};
use legion::prelude::Resources;
use relative_path::RelativePath;

fn resources() -> Resources {
    let mut res = Resources::default();
    res.insert(Actions::default());
    res
}

fn main() -> EngineResult<()> {
    // TODO: load as resource
    let window = WindowConfig {
        inner_size: Some(winit::dpi::Size::Physical(winit::dpi::PhysicalSize::new(
            1920, 1080,
        ))),
        title: "SEED".to_string(),
        ..WindowConfig::default()
    };

    let runtime: RuntimeBuilder<GameState> = Runtime::build(InitState.into())
        .extend_resources(resources())
        .extend_schedule(ActionSystem::system())
        .flush_schedule()
        .extend_schedule(CameraControllerSystem::system(1.0));

    let asset_path = RelativePath::new("asset");

    Engine::start(window, runtime, asset_path)
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
