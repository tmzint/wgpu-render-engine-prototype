pub mod asset;
pub mod base;
pub mod error;
pub mod input;
pub mod material;
pub mod model;
pub mod render;
pub mod texture;

use crate::engine::asset::Assets;
use crate::engine::material::{Materials, Shaders};
use crate::engine::model::Models;
use crate::engine::texture::Textures;
use base::Frame;
use crossbeam_channel::Receiver;
use error::EngineResult;
use futures::executor::block_on;
use input::{InputEvent, Inputs};
use legion::prelude::*;
use notify::{RecommendedWatcher, RecursiveMode, Watcher};
use relative_path::RelativePath;
use render::{Camera, RenderSystem, ResizeViewportSystem, Viewport};
use serde::export::PhantomData;
use std::path::Path;
use std::time::Instant;
use winit::dpi::Size;
use winit::{
    event::{Event, WindowEvent},
    event_loop::{ControlFlow, EventLoop},
    window::{Window, WindowBuilder},
};

pub enum StateInstruction<T> {
    Stay,
    Switch(T),
}

pub trait State
where
    Self: Sized,
{
    fn on_start(&mut self, world: &mut World, resources: &mut Resources) -> StateInstruction<Self>;

    fn on_process(
        &mut self,
        world: &mut World,
        resources: &mut Resources,
    ) -> StateInstruction<Self>;

    fn on_stop(&mut self, world: &mut World, resources: &mut Resources);
}

pub struct HierarchicalState<T: State + Send + Sync + 'static> {
    hierarchy: Vec<T>,
}

impl<T: State + Send + Sync + 'static> HierarchicalState<T> {
    pub fn new(t: T, world: &mut World, resources: &mut Resources) -> Self {
        let mut new = Self { hierarchy: vec![t] };

        let instruction = new
            .active_mut()
            .map(|active| active.on_start(world, resources));

        if let Some(instruction) = instruction {
            new.handle_instruction(instruction, world, resources);
        }

        new
    }

    pub fn active(&self) -> Option<&T> {
        self.hierarchy.last()
    }

    fn active_mut(&mut self) -> Option<&mut T> {
        self.hierarchy.last_mut()
    }

    fn process(&mut self, world: &mut World, resources: &mut Resources) {
        let instruction = self
            .active_mut()
            .map(|active| active.on_process(world, resources));

        if let Some(instruction) = instruction {
            self.handle_instruction(instruction, world, resources);
        }
    }

    fn handle_instruction(
        &mut self,
        instruction: StateInstruction<T>,
        world: &mut World,
        resources: &mut Resources,
    ) {
        let mut instruction = Some(instruction);
        while let Some(i) = instruction.take() {
            match i {
                StateInstruction::Switch(t) => {
                    if let Some(mut stopped) = self.hierarchy.pop() {
                        stopped.on_stop(world, resources);
                    }

                    self.hierarchy.push(t);
                    instruction = self
                        .active_mut()
                        .map(|active| active.on_start(world, resources))
                }
                StateInstruction::Stay => {}
            }
        }
    }
}

pub enum ScheduleInstruction {
    Flush,
    System(Box<dyn Schedulable>),
}

pub struct RuntimeBuilder<T: State + Send + Sync + 'static> {
    state: T,
    schedule_instructions: Vec<ScheduleInstruction>,
    resources: Resources,
}

impl<T: State + Send + Sync + 'static> RuntimeBuilder<T> {
    fn new(t: T) -> RuntimeBuilder<T> {
        Self {
            state: t,
            schedule_instructions: Vec::default(),
            resources: Resources::default(),
        }
    }

    pub fn extend_resources(mut self, res: Resources) -> Self {
        self.resources.merge(res);
        self
    }

    pub fn extend_schedule(mut self, system: Box<dyn Schedulable>) -> Self {
        self.schedule_instructions
            .push(ScheduleInstruction::System(system));
        self
    }

    pub fn flush_schedule(mut self) -> Self {
        self.schedule_instructions.push(ScheduleInstruction::Flush);
        self
    }

    pub fn finish(
        self,
        viewport: Viewport,
        asset_path: &RelativePath,
        notify_rx: Receiver<notify::Event>,
        input_rx: Receiver<InputEvent>,
    ) -> Runtime<T> {
        let universe = Universe::new();
        let mut world = universe.create_world();

        let mut resources = self.resources;

        resources.insert(Materials::new(&viewport));
        resources.insert(Materials::storage());
        resources.insert(Models::storage());
        resources.insert(Shaders::storage());
        resources.insert(Textures::storage());
        resources.insert(Inputs::default());
        resources.insert(Frame::default());
        resources.insert(viewport);

        let assets = Assets::init(std::env::current_dir().unwrap(), asset_path);
        resources.insert(assets);

        let state = HierarchicalState::new(self.state, &mut world, &mut resources);
        resources.insert(state);

        let schedule = Schedule::builder()
            .add_system(Inputs::fetch_system(input_rx))
            .add_system(Assets::system(notify_rx))
            .flush()
            .add_system(ResizeViewportSystem::system())
            .add_system(Textures::system())
            .add_system(Shaders::system())
            .flush()
            .add_system(Materials::system())
            .add_system(Models::system());

        let schedule =
            self.schedule_instructions
                .into_iter()
                .fold(schedule, |schedule, instruction| match instruction {
                    ScheduleInstruction::System(system) => schedule.add_system(system),
                    ScheduleInstruction::Flush => schedule.flush(),
                });

        let schedule = schedule
            .flush()
            .add_system(RenderSystem::system())
            .add_system(Inputs::clear_system())
            .build();

        Runtime {
            universe,
            world,
            resources,
            schedule,
            _state_type: PhantomData,
        }
    }
}

pub struct Runtime<T: State + Send + Sync + 'static> {
    universe: Universe,
    world: World,
    resources: Resources,
    schedule: Schedule,
    _state_type: PhantomData<T>,
}

impl<T: State + Send + Sync + 'static> Runtime<T> {
    pub fn build(t: T) -> RuntimeBuilder<T> {
        RuntimeBuilder::new(t)
    }

    pub fn process(&mut self, delta: f32) -> EngineResult<()> {
        if let Some(mut frame) = self.resources.get_mut::<Frame>() {
            frame.advance(delta)
        }

        if let Some(mut state) = self.resources.remove::<HierarchicalState<T>>() {
            state.process(&mut self.world, &mut self.resources);
            self.resources.insert(state);
        }

        self.schedule.execute(&mut self.world, &mut self.resources);

        Ok(())
    }
}

#[derive(Clone, Debug, PartialEq)]
pub enum Fullscreen {
    Window,
    // TODO: Borderless,
}

#[derive(Clone, Debug, PartialEq)]
pub struct WindowConfig {
    pub resizable: bool,
    pub fullscreen: Fullscreen,
    pub title: String,
    pub inner_size: Option<Size>,
    // TODO: icon
}

impl Default for WindowConfig {
    fn default() -> Self {
        WindowConfig {
            resizable: true,
            fullscreen: Fullscreen::Window,
            title: "untitled".to_string(),
            inner_size: None,
        }
    }
}

pub struct Engine;

impl Engine {
    pub fn start<T: State + Send + Sync + 'static>(
        window: WindowConfig,
        runtime: RuntimeBuilder<T>,
        asset_path: &RelativePath,
    ) -> EngineResult<()> {
        let event_loop = EventLoop::new();

        let window_builder = WindowBuilder::new()
            .with_resizable(window.resizable)
            .with_fullscreen(match window.fullscreen {
                Fullscreen::Window => None,
            })
            .with_title(window.title);

        let window_builder = match window.inner_size {
            Some(size) => window_builder.with_inner_size(size),
            None => window_builder,
        };

        let window = window_builder.build(&event_loop)?;

        let viewport = block_on(Self::viewport(&window))?;

        let (notify_tx, notify_rx) = crossbeam_channel::unbounded::<notify::Event>();
        let mut watcher: RecommendedWatcher =
            notify::immediate_watcher(move |event_res: notify::Result<notify::Event>| {
                notify_tx.send(event_res.unwrap()).unwrap();
            })?;
        watcher.watch(asset_path.to_path(Path::new(".")), RecursiveMode::Recursive)?;

        let (input_tx, input_rx) = crossbeam_channel::unbounded::<InputEvent>();

        let mut runtime = runtime.finish(viewport, asset_path, notify_rx, input_rx);

        let mut prev_time = Instant::now();
        event_loop.run(move |event, _, control_flow| match event {
            Event::RedrawRequested(_) => {
                let now = Instant::now();
                if prev_time < now {
                    let delta = now - prev_time;
                    runtime.process(delta.as_secs_f32()).unwrap();
                }
                prev_time = now;
            }
            Event::MainEventsCleared => {
                window.request_redraw();
            }
            Event::WindowEvent {
                ref event,
                window_id,
            } if window_id == window.id() => match InputEvent::resolve(event) {
                Some(input_event) => {
                    input_tx.send(input_event).unwrap();
                }
                None => match event {
                    WindowEvent::CloseRequested => {
                        *control_flow = ControlFlow::Exit;
                    }
                    _ => (),
                },
            },
            _ => {}
        });
    }

    async fn viewport(window: &Window) -> EngineResult<Viewport> {
        let mut viewport = Viewport::window(window).await?;

        // TODO: make configurable, with the whole viewport?
        let camera = Camera {
            // position the camera one unit up and 2 units back
            // +z is out of the screen
            eye: (0.0, 1.0, 2.0).into(),
            // have it look at the origin
            target: (0.0, 0.0, 0.0).into(),
            // which way is "up"
            up: cgmath::Vector3::unit_y(),
            scale: (1.0, 1.0, 1.0).into(),
            aspect: viewport.aspect(),
            fovy: 45.0,
            znear: 0.1,
            zfar: 100.0,
        };

        viewport.activate_camera(camera);

        Ok(viewport)
    }
}
