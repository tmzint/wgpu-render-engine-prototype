use crate::engine::base::Transform;
use crate::engine::material::MaterialRef;
use crate::engine::model::ModelRef;
use crate::engine::render::Render;
use crate::engine::texture::TextureRef;
use crate::engine::{State, StateInstruction};
use cgmath::prelude::*;
use enclose::enclose;
use enum_dispatch::enum_dispatch;
use legion::prelude::{Resources, World};

#[enum_dispatch(GameState)]
trait ProxyState {
    fn on_start(
        &mut self,
        _world: &mut World,
        _resources: &mut Resources,
    ) -> StateInstruction<GameState> {
        StateInstruction::Stay
    }

    fn on_process(
        &mut self,
        _world: &mut World,
        _resources: &mut Resources,
    ) -> StateInstruction<GameState> {
        StateInstruction::Stay
    }

    fn on_stop(&mut self, _world: &mut World, _resources: &mut Resources) {}
}

pub struct InitState;

impl ProxyState for InitState {
    fn on_start(
        &mut self,
        world: &mut World,
        resources: &mut Resources,
    ) -> StateInstruction<GameState> {
        initialize_state(world, resources);
        StateInstruction::Switch(RunState.into())
    }
}

pub struct RunState;

impl ProxyState for RunState {}

#[enum_dispatch]
pub enum GameState {
    Init(InitState),
    Run(RunState),
}

impl State for GameState {
    fn on_start(&mut self, world: &mut World, resources: &mut Resources) -> StateInstruction<Self> {
        ProxyState::on_start(self, world, resources)
    }

    fn on_process(
        &mut self,
        world: &mut World,
        resources: &mut Resources,
    ) -> StateInstruction<Self> {
        ProxyState::on_process(self, world, resources)
    }

    fn on_stop(&mut self, world: &mut World, resources: &mut Resources) {
        ProxyState::on_stop(self, world, resources)
    }
}

const NUM_INSTANCES_PER_ROW: u32 = 10;
const INSTANCE_DISPLACEMENT: cgmath::Vector3<f32> = cgmath::Vector3::new(
    NUM_INSTANCES_PER_ROW as f32 * 0.5,
    0.0,
    NUM_INSTANCES_PER_ROW as f32 * 0.5,
);

pub fn initialize_state(world: &mut World, _resources: &mut Resources) {
    let texture_ref: TextureRef = "asset/tree/happy-tree.png".into();
    let material_ref: MaterialRef = "asset/material/base.mat".into();
    let model_ref: ModelRef = "asset/tree/model.mod".into();
    // let model_ref: ModelRef = "asset/sprite/model.mod".into();

    let mut entities_components = (0..NUM_INSTANCES_PER_ROW)
            .flat_map(|z| {
                (0..NUM_INSTANCES_PER_ROW).map(enclose!(
                    (model_ref, texture_ref, material_ref) move | x | {
                        let render = Render::new(
                            model_ref.clone(),
                            texture_ref.clone(),
                            material_ref.clone(),
                        );

                        let translation = cgmath::Vector3 { x: x as f32, y: 0.0, z: z as f32 } - INSTANCE_DISPLACEMENT;

                        let rotation = if translation.is_zero() {
                            // this is needed so an object at (0, 0, 0) won't get scaled to zero
                            // as Quaternions can effect scale if they're not create correctly
                            cgmath::Quaternion::from_axis_angle(cgmath::Vector3::unit_z(), cgmath::Deg(0.0))
                        } else {
                            cgmath::Quaternion::from_axis_angle(translation.clone().normalize(), cgmath::Deg(45.0))
                        };

                        let transform = Transform {
                            translation,
                            rotation,
                            ..Transform::default()
                        };

                        (render, transform)
                    }
                ))
            })
            .collect::<Vec<_>>();

    // to create depth issues
    entities_components.reverse();

    world.insert((), entities_components);

    // TODO: dynamic components
    // world.insert((), DynamicComponents {});
}
