use euclid::default::Vector2D;
use std::convert::TryInto;
use std::slice::{Iter, IterMut};

#[derive(Debug, Eq, PartialEq)]
pub struct GridEntry<T> {
    point: Vector2D<i32>,
    pub v: T,
}

impl<T> GridEntry<T> {
    pub fn point(&self) -> Vector2D<i32> {
        self.point
    }
}

#[derive(Debug, PartialEq)]
pub struct Grid<T> {
    pub width: usize,
    pub height: usize,
    underlying: Vec<GridEntry<T>>,
}

impl<T> Grid<T> {
    pub fn new<F>(width: usize, height: usize, f: F) -> Self
    where
        F: Fn() -> T,
    {
        let len = width * height;
        let mut underlying = Vec::with_capacity(width * height);
        for i in 0..len {
            let point = Self::resolve_coordinates(width, i);
            underlying.insert(i, GridEntry { point, v: f() })
        }

        Grid {
            width,
            height,
            underlying,
        }
    }

    pub fn get(&self, point: &Vector2D<i32>) -> Option<&GridEntry<T>> {
        match self.resolve_index(point) {
            Some(i) => self.underlying.get(i),
            None => None,
        }
    }

    pub fn get_mut(&mut self, point: &Vector2D<i32>) -> Option<&mut GridEntry<T>> {
        match self.resolve_index(point) {
            Some(i) => self.underlying.get_mut(i),
            None => None,
        }
    }
    pub fn has_point(&self, point: &Vector2D<i32>) -> bool {
        self.resolve_index(point).is_some()
    }

    pub fn iter(&self) -> Iter<'_, GridEntry<T>> {
        self.underlying.iter()
    }

    pub fn iter_mut(&mut self) -> IterMut<'_, GridEntry<T>> {
        self.underlying.iter_mut()
    }

    fn resolve_coordinates(width: usize, i: usize) -> Vector2D<i32> {
        let x = i / width;
        let y = i % width;
        Vector2D::new(x, y).to_i32()
    }

    fn resolve_index(&self, point: &Vector2D<i32>) -> Option<usize> {
        if point.x < 0 || point.y < 0 {
            None
        } else if self.width as i32 <= point.x || self.height as i32 <= point.y {
            None
        } else {
            (self.width as i32 * point.x + point.y).try_into().ok()
        }
    }
}

#[cfg(test)]
mod test {
    use crate::game::util::grid::Grid;

    #[test]
    fn initialize() {
        let grid = Grid::<i32>::new(5, 4, || 1);
        assert_eq!(5, grid.width);
        assert_eq!(4, grid.height);
        for entry in grid.iter() {
            assert_eq!(1, entry.v)
        }
    }
}
