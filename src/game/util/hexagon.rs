use crate::game::util::grid::Grid;
use euclid::default::Vector2D;
use lazy_static::lazy_static;
use serde::{Deserialize, Serialize};
use std::ops::{Add, Sub};

pub struct Axial;
pub struct Cube;

// x is q and y is r
pub type HexVector = euclid::Vector2D<i32, Axial>;
pub type FHexVector = euclid::Vector2D<f64, Axial>;

pub type HexCubeVector = euclid::Vector3D<i32, Cube>;
pub type FHexCubeVector = euclid::Vector3D<f64, Cube>;

lazy_static! {
    static ref SQRT3: f64 = 3_f64.sqrt();
}

const AXIAL_DIRECTIONS: [HexVector; 6] = [
    HexVector::new(1, 0),
    HexVector::new(0, 1),
    HexVector::new(-1, 1),
    HexVector::new(-1, 0),
    HexVector::new(0, -1),
    HexVector::new(1, -1),
];

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum HexagonVariant {
    Pointy,
    Flat,
}

#[derive(Debug, Clone, PartialEq)]
pub struct Hexagon {
    size: f32,
    height: f32,
    width: f32,
    variant: HexagonVariant,
    corner_points: Vec<Vector2D<f32>>,
}

impl Hexagon {
    pub fn flat(size: f32) -> Self {
        Self::new(size, HexagonVariant::Flat)
    }

    pub fn pointy(size: f32) -> Self {
        Self::new(size, HexagonVariant::Pointy)
    }

    pub fn new(size: f32, variant: HexagonVariant) -> Self {
        let height = size * 2f32;
        let width = ((*SQRT3) * size as f64) as f32;
        let corner_points = Self::corner_points(variant, size);

        Hexagon {
            size,
            height,
            width,
            variant,
            corner_points,
        }
    }

    pub fn size(&self) -> f32 {
        self.size
    }

    pub fn height(&self) -> f32 {
        self.height
    }

    pub fn width(&self) -> f32 {
        self.width
    }

    pub fn axial_to_position(&self, hex_vector: HexVector) -> Vector2D<f32> {
        let hex_vector = hex_vector.to_f64();
        match self.variant {
            HexagonVariant::Pointy => {
                let x =
                    self.size as f64 * ((*SQRT3) * hex_vector.x + (*SQRT3) / 2_f64 * hex_vector.y);
                let y = self.size as f64 * (3_f64 / 2_f64 * hex_vector.y as f64);
                Vector2D::new(x as f32, y as f32)
            }
            HexagonVariant::Flat => {
                let x = self.size as f64 * (3_f64 / 2_f64 * hex_vector.x);
                let y =
                    self.size as f64 * ((*SQRT3) / 2_f64 * hex_vector.x + (*SQRT3) * hex_vector.y);
                Vector2D::new(x as f32, y as f32)
            }
        }
    }

    pub fn position_to_f_axial(&self, position: Vector2D<f32>) -> FHexVector {
        let position = position.to_f64();
        match self.variant {
            HexagonVariant::Pointy => {
                let x = (position.x * (*SQRT3) / 3_f64 - position.y / 3_f64) / self.size as f64;
                let y = position.y * 2_f64 / 3_f64 / self.size as f64;
                FHexVector::new(x, y)
            }
            HexagonVariant::Flat => {
                let x = (2_f64 / 3_f64 * position.x) / self.size as f64;
                let y = (-1_f64 / 3_f64 * position.x + (*SQRT3) * position.y) / self.size as f64;
                FHexVector::new(x, y)
            }
        }
    }

    pub fn position_to_axial(&self, position: Vector2D<f32>) -> HexVector {
        Self::axial_round(self.position_to_f_axial(position))
    }

    pub fn axial_distance_to_inner_circle(&self, point: Vector2D<f32>) -> f32 {
        let hex_vector = self.position_to_axial(point);
        let base = self.axial_to_position(hex_vector);
        let diff = point - base;
        (diff.x * diff.x + diff.y * diff.y).sqrt() - self.width / 2_f32
    }

    pub fn axial_round(f_hex_vector: FHexVector) -> HexVector {
        let f_cube = Self::f_axial_to_cube(f_hex_vector);
        let cube = Self::cube_round(f_cube);
        Self::cube_to_axial(cube)
    }

    pub fn hex_distance(a: HexVector, b: HexVector) -> i32 {
        Self::cube_distance(Self::axial_to_cube(a), Self::axial_to_cube(b))
    }

    pub fn hex_direction(i: i32) -> HexVector {
        let normalized = i % 6;
        AXIAL_DIRECTIONS[normalized as usize]
    }

    pub fn hex_to_direction(hex_vector: &HexVector) -> Option<i32> {
        match hex_vector {
            HexVector { x: 1, y: 0, .. } => Some(0),
            HexVector { x: 0, y: 1, .. } => Some(1),
            HexVector { x: -1, y: 1, .. } => Some(2),
            HexVector { x: -1, y: 0, .. } => Some(3),
            HexVector { x: 0, y: -1, .. } => Some(4),
            HexVector { x: 1, y: -1, .. } => Some(5),
            _ => None,
        }
    }

    pub fn corner_point(&self, i: i32) -> Vector2D<f32> {
        let normalized = i % 6;
        self.corner_points[normalized as usize]
    }

    pub fn neighbours() -> &'static [HexVector] {
        &AXIAL_DIRECTIONS
    }

    fn corner_points(variant: HexagonVariant, size: f32) -> Vec<Vector2D<f32>> {
        let offset = match variant {
            HexagonVariant::Pointy => -30,
            HexagonVariant::Flat => 0,
        };

        let mut corner_points = Vec::with_capacity(6);
        for i in 0..6 {
            let angle_deg = 60 * i + offset;
            let angle_rad = std::f64::consts::PI / 180_f64 * angle_deg as f64;
            let corner_point =
                Vector2D::new(size as f64 * angle_rad.cos(), size as f64 * angle_rad.sin());
            corner_points.push(corner_point.to_f32())
        }

        corner_points
    }

    fn cube_distance(a: HexCubeVector, b: HexCubeVector) -> i32 {
        let diff = (a - b).abs();
        (diff.x + diff.y + diff.z) / 2
    }

    fn cube_round(f_hex_vector: FHexCubeVector) -> HexCubeVector {
        let r_hex_vector = f_hex_vector.round().to_i32();
        let diff = (r_hex_vector.to_f64() - f_hex_vector).abs();

        if diff.x > diff.y && diff.x > diff.z {
            let x = -r_hex_vector.y - r_hex_vector.z;
            HexCubeVector::new(x, r_hex_vector.y, r_hex_vector.z)
        } else if diff.y > diff.z {
            let y = -r_hex_vector.x - r_hex_vector.z;
            HexCubeVector::new(r_hex_vector.x, y, r_hex_vector.z)
        } else {
            let z = -r_hex_vector.x - r_hex_vector.y;
            HexCubeVector::new(r_hex_vector.x, r_hex_vector.y, z)
        }
    }

    fn cube_to_axial(hex_vector: HexCubeVector) -> HexVector {
        hex_vector.xz().cast_unit()
    }

    fn f_axial_to_cube(f_hex_vector: FHexVector) -> FHexCubeVector {
        FHexCubeVector::new(
            f_hex_vector.x,
            -f_hex_vector.x - f_hex_vector.y,
            f_hex_vector.y,
        )
    }

    fn axial_to_cube(hex_vector: HexVector) -> HexCubeVector {
        HexCubeVector::new(hex_vector.x, -hex_vector.x - hex_vector.y, hex_vector.y)
    }
}

#[derive(Debug)]
pub struct HexGridSectionIterator<'a, 'b, T> {
    section: &'a Vec<HexVector>,
    grid: &'b HexGrid<T>,
    index: usize,
}

impl<'a, 'b, T> Iterator for HexGridSectionIterator<'a, 'b, T> {
    type Item = &'b HexGridEntry<T>;

    fn next(&mut self) -> Option<Self::Item> {
        while self.index < self.section.len() {
            let curr = self
                .section
                .get(self.index)
                .and_then(|point| self.grid.get(point));
            self.index += 1;
            if let Some(ge) = curr {
                return Some(ge);
            }
        }

        None
    }
}

#[derive(Debug)]
pub struct HexGridSection {
    section: Vec<HexVector>,
}

impl HexGridSection {
    pub fn resolve<'a, 'b, T>(&'a self, grid: &'b HexGrid<T>) -> HexGridSectionIterator<'a, 'b, T> {
        HexGridSectionIterator {
            section: &self.section,
            grid,
            index: 0,
        }
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Serialize, Deserialize, Default)]
pub struct HexDirections(i32, i32, i32, i32, i32, i32);

impl HexDirections {
    pub fn all() -> Self {
        HexDirections(1, 1, 1, 1, 1, 1)
    }

    pub fn is_all(&self) -> bool {
        self.0 > 0 && self.1 > 0 && self.2 > 0 && self.3 > 0 && self.4 > 0 && self.5 > 0
    }

    pub fn not_all(&self) -> bool {
        !self.is_all()
    }

    pub fn not_empty(&self) -> bool {
        !self.is_empty()
    }

    pub fn is_empty(&self) -> bool {
        self.0 == 0 && self.1 == 0 && self.2 == 0 && self.3 == 0 && self.4 == 0 && self.5 == 0
    }

    pub fn is_none(&self) -> bool {
        !(self.0 > 0 || self.1 > 0 || self.2 > 0 || self.3 > 0 || self.4 > 0 || self.5 > 0)
    }

    pub fn is(&self, i: i32) -> bool {
        let ni = i % 6;
        match ni {
            0 => self.0 > 0,
            1 => self.1 > 0,
            2 => self.2 > 0,
            3 => self.3 > 0,
            4 => self.4 > 0,
            5 => self.5 > 0,
            _ => panic!("expected normalized"),
        }
    }

    pub fn contains(&self, direction: &HexVector) -> bool {
        Hexagon::hex_to_direction(direction)
            .map(|i| self.is(i))
            .unwrap_or_default()
    }

    pub fn set_direction(&mut self, direction: &HexVector, n: i32) {
        if let Some(i) = Hexagon::hex_to_direction(direction) {
            match i {
                0 => self.0 = n,
                1 => self.1 = n,
                2 => self.2 = n,
                3 => self.3 = n,
                4 => self.4 = n,
                5 => self.5 = n,
                _ => panic!("expected normalized"),
            }
        }
    }

    pub fn with_direction(&self, direction: &HexVector, n: i32) -> HexDirections {
        let mut new = self.clone();
        new.set_direction(direction, n);
        new
    }

    pub fn iter(&self) -> DirectionsIterator {
        DirectionsIterator(self, 0)
    }
}

impl Add for HexDirections {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        HexDirections {
            0: self.0 + rhs.0,
            1: self.1 + rhs.1,
            2: self.2 + rhs.2,
            3: self.3 + rhs.3,
            4: self.4 + rhs.4,
            5: self.5 + rhs.5,
        }
    }
}

impl Sub for HexDirections {
    type Output = Self;

    fn sub(self, rhs: Self) -> Self::Output {
        HexDirections {
            0: self.0 - rhs.0,
            1: self.1 - rhs.1,
            2: self.2 - rhs.2,
            3: self.3 - rhs.3,
            4: self.4 - rhs.4,
            5: self.5 - rhs.5,
        }
    }
}

pub struct DirectionsIterator<'a>(&'a HexDirections, usize);

impl<'a> Iterator for DirectionsIterator<'a> {
    type Item = HexVector;

    fn next(&mut self) -> Option<Self::Item> {
        while !self.0.is(self.1 as i32) && self.1 <= 5 {
            self.1 += 1;
        }

        if self.1 > 5 {
            return None;
        }

        let direction = Hexagon::hex_direction(self.1 as i32);
        self.1 += 1;
        Some(direction)
    }
}

#[derive(Debug, Eq, PartialEq)]
pub struct HexGridEntry<T> {
    point: HexVector,
    pub v: T,
}

impl<T> HexGridEntry<T> {
    pub fn point(&self) -> HexVector {
        self.point
    }
}

#[derive(Debug, PartialEq)]
pub struct HexGrid<T> {
    complexity: usize,
    diameter: usize,
    size: usize,
    underlying: Grid<Option<HexGridEntry<T>>>,
}

impl<T> HexGrid<T> {
    pub fn new<F>(complexity: usize, f: F) -> Self
    where
        F: Fn() -> T,
    {
        let (underlying, size) = Self::fill_entries(complexity, f);
        HexGrid {
            complexity,
            diameter: complexity * 2 + 1,
            size,
            underlying,
        }
    }

    pub fn complexity(&self) -> usize {
        self.complexity
    }

    pub fn diameter(&self) -> usize {
        self.diameter
    }

    pub fn size(&self) -> usize {
        self.size
    }

    pub fn get(&self, point: &HexVector) -> Option<&HexGridEntry<T>> {
        let n_point = Vector2D::new(
            point.x + self.complexity as i32,
            point.y + self.complexity as i32,
        );
        self.underlying.get(&n_point).and_then(|ge| ge.v.as_ref())
    }

    pub fn get_mut(&mut self, point: &HexVector) -> Option<&mut HexGridEntry<T>> {
        let n_point = Vector2D::new(
            point.x + self.complexity as i32,
            point.y + self.complexity as i32,
        );
        self.underlying
            .get_mut(&n_point)
            .and_then(|ge| ge.v.as_mut())
    }
    pub fn has_point(&self, point: &HexVector) -> bool {
        self.get(point).map(|_ge| true).unwrap_or_default()
    }

    pub fn iter(&self) -> impl Iterator<Item = &HexGridEntry<T>> {
        self.underlying.iter().flat_map(|a| a.v.as_ref())
    }

    pub fn iter_mut(&mut self) -> impl Iterator<Item = &mut HexGridEntry<T>> {
        self.underlying.iter_mut().flat_map(|a| a.v.as_mut())
    }

    pub fn range(&self, point: HexVector, n: usize) -> HexGridSection {
        let n = n as i32;
        let cube = Hexagon::axial_to_cube(point);
        let mut section = Vec::default();
        for x in (-n)..=n {
            for y in (-n).max(-x - n)..=n.min(-x + n) {
                let z = -x - y;
                let target = cube + HexCubeVector::new(x as i32, y as i32, z as i32);
                section.push(Hexagon::cube_to_axial(target));
            }
        }
        HexGridSection { section }
    }

    fn fill_entries<F>(complexity: usize, f: F) -> (Grid<Option<HexGridEntry<T>>>, usize)
    where
        F: Fn() -> T,
    {
        let diameter = complexity * 2 + 1;
        let mut underlying = Grid::new(diameter, diameter, || None);
        let complexity = complexity as i32;
        let mut size = 0;
        for x in -complexity..=complexity {
            for y in -complexity..=complexity {
                if (x + y).abs() <= complexity {
                    let point = HexVector::new(x, y);
                    let n_point = Vector2D::new(point.x + complexity, point.y + complexity);
                    let entry = HexGridEntry { point, v: f() };
                    underlying.get_mut(&n_point).unwrap().v = Some(entry);
                    size += 1;
                }
            }
        }

        (underlying, size)
    }
}

#[cfg(test)]
mod test {
    use crate::game::util::hexagon::{HexDirections, HexGrid, HexVector, Hexagon};
    use std::collections::HashSet;
    use std::iter::FromIterator;

    #[test]
    fn empty_directions_iterator() {
        let directions = HexDirections {
            0: 0,
            1: 0,
            2: 0,
            3: 0,
            4: 0,
            5: 0,
        };
        assert_eq!(
            Vec::<HexVector>::default(),
            directions.iter().collect::<Vec<_>>()
        )
    }

    #[test]
    fn all_directions_iterator() {
        let directions = HexDirections::all();
        assert_eq!(
            Hexagon::neighbours().to_vec(),
            directions.iter().collect::<Vec<_>>()
        );
    }

    #[test]
    fn empty_hex_grid() {
        let grid = HexGrid::new(1, || 1);
        assert!(grid.iter().all(|e| e.v == 1));
        assert_eq!(7, grid.size);
        assert_eq!(7, grid.iter().count());
    }

    #[test]
    fn not_get_rhombus_entry_hex_grid() {
        let mut grid = HexGrid::new(1, || 1);
        assert_eq!(None, grid.get(&HexVector::new(1, 1)));
        assert_eq!(None, grid.get_mut(&HexVector::new(1, 1)));
        assert_eq!(None, grid.get(&HexVector::new(-1, -1)));
        assert_eq!(None, grid.get_mut(&HexVector::new(-1, -1)));
    }

    #[test]
    fn not_get_outside_hex_grid() {
        let mut grid = HexGrid::new(1, || 1);
        assert_eq!(None, grid.get(&HexVector::new(1, -2)));
        assert_eq!(None, grid.get_mut(&HexVector::new(1, -2)));
    }

    #[test]
    fn hex_grid_range() {
        let mut grid = HexGrid::new(3, || 0);
        grid.get_mut(&HexVector::new(0, 0)).unwrap().v = 1;

        let section = grid.range(HexVector::new(0, 0), 0);
        assert_eq!(&vec![HexVector::new(0, 0)], &section.section);
        assert_eq!(
            vec![HexVector::new(0, 0)],
            section
                .resolve(&grid)
                .map(|ge| ge.point)
                .collect::<Vec<_>>()
        );

        let section = grid.range(HexVector::new(0, 0), 1);

        let expected = Hexagon::neighbours()
            .iter()
            .cloned()
            .chain(std::iter::once(HexVector::new(0, 0)))
            .collect::<HashSet<_>>();

        assert_eq!(
            expected,
            HashSet::from_iter(section.section.iter().cloned())
        );

        assert_eq!(
            expected,
            section
                .resolve(&grid)
                .map(|ge| ge.point)
                .collect::<HashSet<_>>()
        );
    }
}
