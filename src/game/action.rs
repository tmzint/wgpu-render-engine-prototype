use crate::engine::input::{InputEvent, Inputs};
use cgmath::Vector2;
use legion::prelude::*;
use winit::event::{ElementState, VirtualKeyCode};

pub struct Actions {
    pub direction: Vector2<i32>,
}

impl Default for Actions {
    fn default() -> Self {
        let direction = Vector2 { x: 0, y: 0 };

        Self { direction }
    }
}

pub struct ActionSystem;

impl ActionSystem {
    pub fn system() -> Box<dyn Schedulable> {
        SystemBuilder::new("ActionSystem")
            .write_resource::<Actions>()
            .read_resource::<Inputs>()
            .build(move |_commands, _world, (actions, inputs), _query| {
                for event in &inputs.events {
                    match event {
                        InputEvent::Resize(_) => (),
                        InputEvent::KeyboardInput {
                            device_id: _,
                            input,
                            is_synthetic: _,
                        } => {
                            if let Some(keycode) = input.virtual_keycode {
                                let is_pressed = input.state == ElementState::Pressed;
                                match keycode {
                                    VirtualKeyCode::W | VirtualKeyCode::Up => {
                                        actions.direction.y = if is_pressed { -1 } else { 0 };
                                    }
                                    VirtualKeyCode::A | VirtualKeyCode::Left => {
                                        actions.direction.x = if is_pressed { 1 } else { 0 };
                                    }
                                    VirtualKeyCode::S | VirtualKeyCode::Down => {
                                        actions.direction.y = if is_pressed { 1 } else { 0 };
                                    }
                                    VirtualKeyCode::D | VirtualKeyCode::Right => {
                                        actions.direction.x = if is_pressed { -1 } else { 0 };
                                    }
                                    _ => (),
                                }
                            }
                        }
                    }
                }
            })
    }
}
