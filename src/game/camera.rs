use crate::engine::base::Frame;
use crate::engine::render::Viewport;
use crate::game::Actions;
use cgmath::Vector3;
use legion::prelude::*;

pub struct CameraControllerSystem;

impl CameraControllerSystem {
    pub fn system(speed: f32) -> Box<dyn Schedulable> {
        SystemBuilder::new("CameraControllerSystem")
            .write_resource::<Viewport>()
            .read_resource::<Actions>()
            .read_resource::<Frame>()
            .build(
                move |_commands, _world, (viewport, actions, frame), _query| {
                    if let Some(active_camera) = viewport.active_camera.as_mut() {
                        let v = actions.direction.cast::<f32>().unwrap() * speed * frame.delta;
                        let v = Vector3 {
                            x: v.x,
                            y: 0.0,
                            z: v.y,
                        };

                        active_camera.eye += v;
                        active_camera.target += v;
                    }
                },
            )
    }
}
