// shader.vert
#version 450

struct Instance {
    mat4 s_model;
    vec3 scale;
};


// IN
layout(location=0) in vec3 a_position;
layout(location=1) in vec2 a_tex_coords;


// OUT
layout(location=0) out vec2 v_tex_coords;


// BINDING
layout(set=1, binding=0) uniform Uniforms { mat4 u_view_proj; };

layout(set=2, binding=0)
buffer Instances {
    Instance instances[];
};


void main() {
    v_tex_coords = a_tex_coords;
    Instance instance;
    instance = instances[gl_InstanceIndex];
    gl_Position = u_view_proj * instance.s_model * vec4(a_position * instance.scale, 1.0);
}

