{
  "vertices": [
    {
      "position": [0, 0, 0.0],
      "tex_coords": [1, 1]
    },
    {
      "position": [1, 0, 0.0],
      "tex_coords": [0, 1]
    },
    {
      "position": [1, 1, 0.0],
      "tex_coords": [0, 0]
    },
    {
      "position": [0, 1, 0.0],
      "tex_coords": [1, 0]
    }
  ],
  "indices": [3, 1, 2, 1, 3, 0]
}
